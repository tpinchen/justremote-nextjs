export const defaultFilterState = {
  all: {
    filtersApplied: false,
    filters: {}
  },
  developer: {
    filtersApplied: false,
    filters: {}
  },
  marketing: {
    filtersApplied: false,
    filters: {}
  },
  manager: {
    filtersApplied: false,
    filters: {}
  },
  design: {
    filtersApplied: false,
    filters: {}
  },
  writing: {
    filtersApplied: false,
    filters: {}
  },
  customerservice: {
    filtersApplied: false,
    filters: {}
  }
};
