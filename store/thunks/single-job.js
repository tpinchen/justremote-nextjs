import { Server } from "../../utils/server";
import { BASE_JOBS_API } from "../../constants/paths";
import {
  fetchSingleJobSuccess,
  fetchingSingleJob,
  fetchSingleJobError
} from "../actions/single-job-actions";
import { showLoader, hideLoader } from "../actions/loading-actions";

const buildRequestURL = jobId => {
  return `${BASE_JOBS_API}/${jobId}`;
};

export const fetchSingleJob = jobId => dispatch => {
  const requestUrl = buildRequestURL(jobId);
  dispatch(fetchingSingleJob());

  const loaderInterval = setTimeout(() => {
    dispatch(showLoader());
  }, 1000);

  return Server({
    method: "get",
    url: requestUrl
  })
    .then(response => response.data)
    .then(job => {
      dispatch(fetchSingleJobSuccess(job));
      dispatch(hideLoader());
      clearTimeout(loaderInterval);
    })
    .catch(err => {
      console.log(err);
      dispatch(fetchSingleJobError(err));
      dispatch(hideLoader());
      clearTimeout(loaderInterval);
    });
};
