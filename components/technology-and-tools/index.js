import React from "react";
import styled from "styled-components";
import { ChipBlock } from "../chip-block";
import { media } from "../../constants/theme";

const StyledTechnologyAndTools = styled.div`
  background-color: #f4f6f7;
  padding: 25px;
  border-radius: 0;
  margin-bottom: 40px;

  ${media.desktop`
    padding: 30px;
    border-radius:3px;
  `}
`;

export const TechnologyAndTools = (props) => {
  const hasTech = props.technologies && props.technologies.length > 0;
  const hasTools = props.tools && props.tools.length > 0;
  const styles = hasTech && hasTools ? { marginBottom: 10 } : {};

  return (
    <div>
      {hasTech || hasTools ? (
        <StyledTechnologyAndTools>
          {hasTech ? (
            <ChipBlock
              styles={styles}
              title={"Technology"}
              chips={props.technologies}
            />
          ) : null}
          {hasTools ? <ChipBlock title={"Tools"} chips={props.tools} /> : null}
        </StyledTechnologyAndTools>
      ) : null}
    </div>
  );
};
