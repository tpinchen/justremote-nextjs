export const OverlapHours = [
  { title: "1 hour", value: 1 },
  { title: "2 hours", value: 2 },
  { title: "3 hours", value: 3 },
  { title: "4 hours", value: 4 },
  { title: "5 hours", value: 5 },
  { title: "6 hours", value: 6 },
  { title: "7 hours", value: 7 },
  { title: "8 hours", value: 8 },
];
