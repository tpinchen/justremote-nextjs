import React from "react";
import styled from "styled-components";
import { media, colors } from "../../constants/theme";

const StyledJobRestrictions = styled.div`
  text-align: center;
  background-color: ${colors.backgroundBlue};
  padding: 30px 20px;
  font-style: italic;
  font-size: 14px;
  font-weight: 400;

  ${media.desktop`
    padding: 40px 30px;
    border-radius: 3px;
    margin-bottom: 40px;
  `}
`;

const P = styled.p`
  margin: 0;
`;

const Location = styled.span`
  display: inline-flex;
`;

const renderCountries = (countries) => {
  const fullLength = countries.length;

  return countries.map((country, i) => {
    let index = i + 1;
    return (
      <span key={i}>
        {country.label}
        {index !== fullLength ? ", " : ""}
      </span>
    );
  });
};

export const JobRestrictions = (props) => {
  let job_restrictions;
  if (props.remote_type === "Partially Remote") {
    job_restrictions = (
      <P>
        <b>Partially Remote:</b> Applicants must be within a commutable distance
        of {props.location ? props.location : "[Job Location]"}
      </P>
    );
  } else if (props.requires_specific_country) {
    job_restrictions = (
      <P>
        Only accepting applications from:{" "}
        {props.country_list.length > 0
          ? renderCountries(props.country_list)
          : ""}
      </P>
    );
  } else if (props.requires_time_overlap) {
    job_restrictions = (
      <P>
        Applicants must be able to work{" "}
        {props.overlap_hours === 1
          ? `a ${props.overlap_hours} hour`
          : `${props.overlap_hours} hours`}{" "}
        overlap with {props.company_name}'s working day{" "}
        <Location>
          ({props.location ? props.location : "Job Location"})
        </Location>
      </P>
    );
  } else {
    job_restrictions = <P>Open to all applicants globally!</P>;
  }

  return <StyledJobRestrictions>{job_restrictions}</StyledJobRestrictions>;
};
