import axios from "axios";

export const Server = (props) => {
  // const URL_BASE =
  //   process.env.NODE_ENV === "development"
  //     ? "http://localhost:8080"
  //     : "https://lq07.hatchboxapp.com";
  const URL_BASE = "https://lq07.hatchboxapp.com";

  return axios({
    method: props.method,
    url: `${URL_BASE}${props.url}`,
    data: props.data ? props.data : null,
  });
};
