import { paths } from "../constants/paths";

export const isWritingRoute = (location) => {
  if (location.pathname === paths.writing_jobs) return true;
  if (location.pathname === paths.editing_jobs) return true;
  if (location.pathname === paths.copywriter_jobs) return true;
  return false;
};

export const isManagerRoute = (location) => {
  if (location.pathname === paths.manager_jobs) return true;
  if (location.pathname === paths.recruiter_jobs) return true;
  if (location.pathname === paths.sales_jobs) return true;
  if (location.pathname === paths.hr_jobs) return true;
  return false;
};

export const isMarketingRoute = (location) => {
  if (location.pathname === paths.marketing_jobs) return true;
  if (location.pathname === paths.social_media_jobs) return true;
  if (location.pathname === paths.seo_jobs) return true;
  return false;
};
