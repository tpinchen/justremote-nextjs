import styled from "styled-components";
import Head from "next/head";
import { PageWrapper } from "../components/page-wrapper";
import { media } from "../constants/theme";

const Hero = styled.div`
  display: flex;
  background-image: url("/contact-image-min.jpg");
  background-size: cover;
  background-position: bottom;

  height: calc(60vh - 60px);
  justify-content: center;
  align-items: center;

  ${media.desktop`
    height: calc(100vh - 60px);
  `}
`;

const Headline = styled.h1`
  background-color: #1b1b1b;
  color: #fff;
  padding: 10px;
  text-align: center;
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 3px;
  line-height: 1.8;
  margin-left: 15px;
  margin-right: 15px;

  ${media.desktop`
    position:relative;
    top: 100px;
    color: #fff;
    font-size: 14px;
    padding: 15px 25px;
    display:block;
    font-weight: 300;
    line-height: 1.4;
  `}
`;

const Copy = styled.div`
  max-width: 700px;
  width: calc(100% - 30px);
  margin: 50px auto 50px;
`;

const Subtitle = styled.h2`
  font-size: 20px;
  margin-bottom: 8px;
  font-weight: 600;
`;

export const Contact = () => {
  return (
    <div>
      <Head>
        <title>Contact us - JustRemote</title>
        <meta
          name="description"
          content="We love to get feedback and comments from our users. If you have questions, are keen to post a remote job or simply want to speak with us then get in touch here"
        />
      </Head>
      <PageWrapper>
        <Hero>
          <Headline>Have questions? We love to hear from you</Headline>
        </Hero>

        <Copy>
          <Subtitle>Get in touch</Subtitle>
          <p>
            To get in touch with us about anything please email{" "}
            <a href="mailto:contact@justremote.com">contact@justremote.co</a>
          </p>
          <p>We aim to reply to all emails within 24 hours.</p>
        </Copy>
      </PageWrapper>
    </div>
  );
};

export default Contact;
