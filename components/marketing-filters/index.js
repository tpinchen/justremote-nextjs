import React from "react";
import { withRouter } from "next/router";
import { paths } from "../../constants/paths";
import { LinkChip } from "../../atoms/chip";
import { Filter } from "../../atoms/filter";
import { FilterLabel } from "../../atoms/filter-label";
import { isMarketingRoute } from "../../utils/route-helpers";

export const MarketingFilters = ({ location }) => (
  <>
    {isMarketingRoute(location) && (
      <Filter>
        <FilterLabel>Sub-Categories</FilterLabel>
        <LinkChip
          selected={location.pathname === paths.marketing_jobs}
          label="All Marketing"
          path={paths.marketing_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.seo_jobs}
          label="SEO"
          path={paths.seo_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.social_media_jobs}
          label="Social Media"
          path={paths.social_media_jobs}
        />
      </Filter>
    )}
  </>
);

export default withRouter(MarketingFilters);
