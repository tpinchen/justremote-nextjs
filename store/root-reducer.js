import { combineReducers } from "redux";
import { reducers } from "./reducers";

const appReducer = combineReducers({
  ...reducers,
});

export const rootReducer = (state, action) => {
  return appReducer(state, action);
};
