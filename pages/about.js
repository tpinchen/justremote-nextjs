import styled from "styled-components";
import { PageWrapper } from "../components/page-wrapper";
import { media } from "../constants/theme";
import Head from "next/head";

const Hero = styled.div`
  display: flex;
  background-image: url("/about-image-min.jpg");
  background-size: cover;
  background-position: center;
  height: calc(60vh - 60px);
  justify-content: center;
  align-items: center;

  ${media.desktop`
    height: calc(100vh - 60px);
  `}
`;

const Headline = styled.h1`
  background-color: #1b1b1b;
  color: #fff;
  padding: 10px;
  text-align: center;
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 3px;
  line-height: 1.8;
  margin-left: 15px;
  margin-right: 15px;

  ${media.desktop`
    position:relative;
    top: 100px;
    color: #fff;
    font-size: 14px;
    padding: 15px 25px;
    display:block;
    font-weight: 300;
    line-height: 1.4;
    margin-left: 0;
    margin-right: 0;
  `}
`;

const Mission = styled.h2`
  background-color: #fff;
  padding: 20px;
  position: relative;
  top: -20px;
  max-width: 700px;
  width: calc(100% - 30px);
  display: block;
  box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.15);
  border-radius: 3px;
  text-align: center;
  font-weight: 300;
  font-size: 16px;
  margin: 0 auto;

  ${media.desktop`
    top: -50px;
    font-size: 20px;
  `}
`;

const Copy = styled.div`
  max-width: 700px;
  width: calc(100% - 30px);
  margin: 0 auto 50px;
`;

const Subtitle = styled.h2`
  font-size: 20px;
  margin-bottom: 8px;
  font-weight: 600;
`;

const P = styled.p`
  margin-bottom: 15px;
  font-size: 18px;
`;

const About = () => {
  return (
    <div>
      <Head>
        <title>
          Why we've created a better Remote Job Platform - JustRemote
        </title>
        <meta
          name="description"
          content="Find out about JustRemote and why we've created a remote job platform that is better than any other."
        />
      </Head>
      <PageWrapper>
        <Hero>
          <Headline>Do what you love, daily, from anywhere</Headline>
        </Hero>
        <Mission>
          Our mission is to help you find work that you love and enable you to
          do it daily from anywhere.
        </Mission>

        <Copy>
          <Subtitle>Why we exist</Subtitle>
          <P>
            Communication tools are better than ever with email, chat and video
            calls allowing for instant communication with anyone around the
            globe at the click of a button or the push of the enter key.
          </P>
          <P>
            Simply put there is no better time in history to have a distributed
            workforce or be a remote worker than at this very moment and with
            every day that passes things are only getting better.
          </P>
          <P>
            We want to help everyone find a job that they love and allow them to
            do it from anywhere.
          </P>
        </Copy>
      </PageWrapper>
    </div>
  );
};

export default About;
