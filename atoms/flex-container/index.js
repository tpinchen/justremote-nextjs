import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledFlexContainer = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  margin: ${(props) => (props.margin ? props.margin.mobile : null)};

  ${media.desktop`
    margin: ${(props) => (props.margin ? props.margin.desktop : null)};
  `}
`;

export const FlexContainer = (props) => {
  return (
    <StyledFlexContainer margin={props.margin}>
      {props.children}
    </StyledFlexContainer>
  );
};
