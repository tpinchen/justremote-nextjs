import React from "react";
import styled from "styled-components";
import LazyLoad from "react-lazyload";
import { LinkButton } from "../../atoms/button";
import { paths } from "../../constants/paths";
import { media } from "../../constants/theme";
import image from "../../../assets/images/new-job-mountains-min.jpg";

const NewJobWrapper = styled.div`
  ${media.desktop`
    height: 400px;
    background-image: url(${image});
    background-size:cover;
    background-position-y: 650px;
    width: 100%;
    margin:0 auto;
    display:flex;
    flex-direction:column;
    align-items: center;
    justify-content: center;
    clip-path: polygon(0 0,100% 10%,100% 100%,0 100%)
  `}
`;
const ButtonWrapper = styled.div`
  text-align: center;
  position: relative;
  top: -20px;

  ${media.desktop`
    top: 0;
    order: 2;
  `}
`;
const JobCopy = styled.p`
  text-align: center;
  margin-bottom: 30px;
  font-size: 18px;

  ${media.desktop`
    order: 1;
    font-size: 14px;
    color: #fff;
    background-color: #1b1b1b;
    padding: 15px;
    font-weight: 400;
    text-transform:uppercase;
    letter-spacing: 3px;

  `}
`;

const MobileImage = styled.img`
  display: block;
  width: 100%;

  ${media.desktop`
    display:none;
  `}
`;

export class NewJob extends React.Component {
  render() {
    return (
      <LazyLoad offset={400} once>
        <NewJobWrapper>
          <MobileImage src={image} alt="Mountain range" />
          <ButtonWrapper>
            <LinkButton
              isGreen
              title="Post a Job"
              path={paths.post_job}
              align={"center"}
            />
          </ButtonWrapper>
          <JobCopy>
            Reach over 20,000+ professional remote workers in the next 30 days
          </JobCopy>
        </NewJobWrapper>
      </LazyLoad>
    );
  }
}
