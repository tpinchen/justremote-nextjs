import Head from "next/head";
import styled from "styled-components";
import NewJobPreviewDetails from "../new-job-preview-details";
import { isFavourited } from "../../utils/favourites";
import { PageWrapper } from "../page-wrapper";
import { JobInactive } from "../job-inactive";
import { FavouriteButton } from "../../atoms/favourite-button";
import { withRouter } from "next/router";
import { useDispatch } from "react-redux";
import { setFavourite } from "../../store/actions";

const PreviewWrapper = styled.div`
  margin-top: 40px;
  margin-bottom: 40px;
`;

export const ShowSingleJob = ({ job, favourited, pageTitle, pageDesc }) => {
  const dispatch = useDispatch();

  const createFavouriteJobObj = () => {
    const {
      category,
      company_name,
      date,
      href,
      id,
      job_type,
      title,
      remote_type,
    } = job;

    return {
      category: category,
      company_name: company_name,
      date: date,
      href: href,
      id: id,
      job_type: job_type,
      title: title,
      remote_type: remote_type,
    };
  };

  return (
    <div>
      <Head>
        <title>{pageTitle}</title>
        <meta name="description" content={pageDesc} />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/medium-draft.css"
          // href="https://unpkg.com/medium-draft/dist/medium-draft.css"
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/basic.css"
          // href="https://unpkg.com/medium-draft/dist/basic.css"
        />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@justremoteco" />
        <meta
          name="twitter:title"
          content={
            job && job.title ? `${job.title} at ${job.company_name}` : ""
          }
        />
        <meta name="twitter:description" content={pageDesc} />
        <meta
          name="twitter:image"
          content={
            job && job.company_logo
              ? `${job.company_logo.url}`
              : "https://remoteworker-live-superbrnds.s3.eu-west-2.amazonaws.com/Shareable/just_remote_logo.jpg"
          }
        />
      </Head>
      <PageWrapper>
        {Object.keys(job).length > 0 && job.constructor === Object && (
          <div>
            {job.inactive ? (
              <JobInactive job={job} />
            ) : (
              <div>
                <PreviewWrapper>
                  <NewJobPreviewDetails job={job} />
                </PreviewWrapper>
                <FavouriteButton
                  onClick={() =>
                    dispatch(setFavourite(createFavouriteJobObj()))
                  }
                  favourited={isFavourited(job.id, favourited)}
                />
              </div>
            )}
          </div>
        )}
      </PageWrapper>
    </div>
  );
};

export default withRouter(ShowSingleJob);
