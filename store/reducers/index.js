import homeJobsReducer from "./home-job-reducer";
import favouriteReducer from "./favourite-reducer";
import newJobReducer from "./new-job-reducer";
import jobsReducer from "./jobs-reducer";
import filterReducer from "./filter-reducer";
import singleJobReducer from "./single-job-reducer";
import messengerReducer from "./messenger-reducer";
import loadingReducer from "./loading-reducer";
import articleReducer from "./article-reducer";
import companiesReducer from "./companies-reducer";
import companyReducer from "./company-reducer";
import emailFormReducer from "./email-form-reducer";
import resourceReducer from "./resource-reducer";
import personReducer from "./person-reducer";
import authReducer from "./auth-reducer";
import modalReducer from "./modal-reducer";

export const reducers = {
  homeJobsState: homeJobsReducer,
  favouriteState: favouriteReducer,
  newJobState: newJobReducer,
  jobsState: jobsReducer,
  singleJobState: singleJobReducer,
  messengerState: messengerReducer,
  loadingState: loadingReducer,
  filterState: filterReducer,
  articleState: articleReducer,
  companiesState: companiesReducer,
  companyState: companyReducer,
  emailFormState: emailFormReducer,
  resourceState: resourceReducer,
  personState: personReducer,
  authState: authReducer,
  modalState: modalReducer
};
