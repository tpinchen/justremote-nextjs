import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Snackbar, { SnackbarContent } from "material-ui/Snackbar";

const styles = {
  root: {
    backgroundColor: "#1b1b1b",
    fontFamily: "'europa', sans-serif",
    fontSize: 15,
  },
};

class StyledMessenger extends Component {
  handleClose = () => {
    if (this.props.message.open) this.props.closeMessage();
  };

  render() {
    const { classes, message } = this.props;
    return (
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={message.open}
        onClose={this.handleClose}
      >
        <SnackbarContent
          classes={{
            root: classes.root,
          }}
          message={message.message}
        />
      </Snackbar>
    );
  }
}

export const Messenger = withStyles(styles)(StyledMessenger);
