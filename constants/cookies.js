export const COOKIE_NAMES = {
  EMAIL_FORM_DISMISSED: "_rw_ef_dismissed",
  EMAIL_FORM_SUBMITTED: "_rw_ef_submitted",
  ADMIN: "_rw_a",
  TOKEN: "_jr_tok",
};
