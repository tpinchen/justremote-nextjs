import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.marketing.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.marketing.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="social media"
      canonical={`https://justremote.co${paths.social_media_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Social Media Jobs - JustRemote"
      pageDesc="Remote Social Media Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = response.data.filter(
    (job) => job.title.toLowerCase().indexOf("social") >= 0
  );

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
