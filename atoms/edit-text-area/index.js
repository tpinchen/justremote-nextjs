import React, { Component } from "react";
import styled from "styled-components";
import mediumDraftExporter from "medium-draft/lib/exporter";
import { Editor, createEditorState } from "medium-draft";
import { ErrorMessage } from "../error-message";

const StyledEditor = styled.div`
  .md-RichEditor-root {
    padding: 0 15px 15px 15px;
    border: 1px solid ${props => (props.validate ? "red" : "#ddd")};
    border-radius: 3px;
  }
`;

export class EditTextArea extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialised: false
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.setState({
      initialised: true,
      editorState: createEditorState()
    });
  }

  handleChange(editorState) {
    this.setState({ editorState }, () => {
      let html = mediumDraftExporter(
        this.state.editorState.getCurrentContent()
      );
      this.props.change(html);
    });
  }

  render() {
    if (this.state.initialised) {
      return (
        <StyledEditor validate={this.props.validate}>
          <Editor
            editorState={this.state.editorState}
            onChange={value => this.handleChange(value)}
            placeholder=""
            sideButtons={[]}
            toolbarConfig={{
              block: ["ordered-list-item", "unordered-list-item"],
              inline: this.props.hyperlink
                ? ["BOLD", "ITALIC", "UNDERLINE", "hyperlink"]
                : ["BOLD", "ITALIC", "UNDERLINE"]
            }}
          />
          {this.props.validate ? (
            <ErrorMessage message={this.props.validate.message} />
          ) : null}
        </StyledEditor>
      );
    }
    return null;
  }
}
