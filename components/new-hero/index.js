import React from "react";
import styled from "styled-components";
import { media, colors } from "../../constants/theme";

const HeroWrapper = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  ${media.desktop`
    min-height: 90vh;  
  `}
`;

const HeroContent = styled.div`
  color: #1b1b1b;
  text-align: center;
  padding: 0 16px;
  z-index: 4;
  position: relative;

  ${media.desktop`
    padding: 0 24px;    
  `}
`;

const HeroLink = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 0;
  margin-top: 24px;

  > div {
    background: ${colors.primaryRed};
    color: #fff;
    font-weight: 600;
    width: 200px;
    height: 50px;
    line-height: 16px;
    border-radius: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-transform: capitalize;

    &:hover {
      cursor: pointer;
    }
  }
`;

const Headline = styled.h1`
  font-size: 32px;
  letter-spacing: -0.05em;
  margin: 0;
  line-height: 1em;
  margin-bottom: 14px;
  text-transform: capitalize;

  ${media.desktop`
    font-size: 40px;
  `}
`;
const SubHeading = styled.p`
  font-size: 16px;
  color: #b0b0b0;
  font-weight: 300;
  line-height: 23px;
  margin: 0;
  ${media.desktop`
    font-size: 18px;
  `}
`;

const LinkText = styled.p`
  color: ${colors.linkBlue};
  margin: 0;
  &:hover {
    cursor: pointer;
  }
`;

const Or = styled.p`
  margin: 8px 0;
  ${media.desktop`
    margin: 16px 0;
  `}
`;

export default (props) => {
  const setHeading = () => {
    const { category } = props;
    if (category === "customerservice") {
      return "customer service";
    }

    if (category === "seo") {
      return "SEO";
    }

    if (category === "manager") {
      return "manager/exec";
    }
    if (category === "hr") {
      return "HR";
    }

    return category;
  };

  const isHome = props.category === "";

  return (
    <HeroWrapper>
      <HeroContent>
        <Headline>
          Remote {setHeading()} Jobs {isHome && "That Fit Your Life"}
        </Headline>
        <SubHeading>
          Discover fully and partially remote jobs from the greatest remote
          working companies
        </SubHeading>
        <HeroLink>
          {/* <Link
              href={paths.all_jobs}
              onClick={() =>
                TrackClick({
                  category: "Navigation - Homepage",
                  action: "Category Link",
                  label: "All",
                })
              }
            > */}
          <div onClick={props.scrollToJobs}>View Jobs</div>
          {/* </Link> */}
        </HeroLink>
        <Or>or</Or>
        <LinkText onClick={props.handleScrollClick}>
          Create a Remote Resume
        </LinkText>
      </HeroContent>
    </HeroWrapper>
  );
};
