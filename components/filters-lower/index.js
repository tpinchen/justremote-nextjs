import React from "react";
import styled from "styled-components";
import { RadioChips } from "../../atoms/radio-chips";

const StyledFiltersLower = styled.div`
  margin-top: 0px;
  margin-bottom: 15px;
`;

const buildChipList = (jobs, prop) => {
  return jobs.map(job => job[prop]);
};

const dedupeData = (jobs, prop) => {
  return buildChipList(jobs, prop)
    .filter((elem, pos, arr) => {
      return arr.indexOf(elem) === pos;
    })
    .sort();
};

export const FiltersLower = props => {
  const {
    filterCat,
    addFilter,
    removeFilter,
    category,
    filters,
    jobs,
    removeFromFilters,
    addToFilters
  } = props;

  const handleClick = (chip, chipType) => {
    const filter = {
      category: category,
      filterCat: filterCat,
      data: chip.selected && chipType === "Radio" ? null : chip.label
    };
    if (chip.selected) {
      chipType === "Radio" ? removeFilter(filter) : removeFromFilters(filter);
    } else {
      chipType === "Radio" ? addFilter(filter) : addToFilters(filter);
    }
  };

  const content = () => {
    if (filterCat === "job_type") {
      return (
        <RadioChips
          chips={[
            {
              label: "Permanent",
              selected:
                filters.job_type && filters.job_type === "Permanent"
                  ? true
                  : false
            },
            {
              label: "Contract",
              selected:
                filters.job_type && filters.job_type === "Contract"
                  ? true
                  : false
            }
          ]}
          click={chip => handleClick(chip, "Radio")}
        />
      );
    }

    if (filterCat === "remote_type") {
      return (
        <RadioChips
          chips={[
            {
              label: "Fully Remote",
              selected:
                filters.remote_type && filters.remote_type === "Fully Remote"
                  ? true
                  : false
            },
            {
              label: "Partially Remote",
              selected:
                filters.remote_type &&
                filters.remote_type === "Partially Remote"
                  ? true
                  : false
            }
          ]}
          click={chip => handleClick(chip, "Radio")}
        />
      );
    }
    return null;
  };

  if (filterCat === "job_country") {
    let countryList = dedupeData(jobs, "job_country");
    return (
      <RadioChips
        chips={countryList.map(country => {
          return {
            label: country,
            selected:
              filters.job_country &&
              filters.job_country.find(location => location === country)
                ? true
                : false
          };
        })}
        click={chip => handleClick(chip, "Checkbox")}
      />
    );
  }

  if (filterCat === "technology") {
    // Get a list of all the unique technologies
    const dedupeTechnologies = jobs => {
      let uniqTechnologyList = [];
      jobs.map(job => {
        let techList = job.technology_list;
        if (techList && techList.length > 0) {
          return techList.map(tech => {
            if (uniqTechnologyList.indexOf(tech.label) === -1)
              uniqTechnologyList.push(tech.label);
            return null;
          });
        }
        return null;
      });
      uniqTechnologyList.sort();
      return uniqTechnologyList;
    };

    let technologyList = dedupeTechnologies(jobs);

    return (
      <RadioChips
        chips={technologyList.map(technology => {
          return {
            label: technology,
            selected:
              filters.technology &&
              filters.technology.find(tech => tech === technology)
                ? true
                : false
          };
        })}
        click={chip => handleClick(chip, "Checkbox")}
      />
    );
  }

  return <StyledFiltersLower>{content()}</StyledFiltersLower>;
};
