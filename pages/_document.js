import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";
export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => {
            return sheet.collectStyles(<App {...props} />);
          },
        });

      const initialProps = await Document.getInitialProps(ctx);
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=5.0"
          />
          <meta
            name="p:domain_verify"
            content="053c43aa42a7d52ed8ffb1dbbfc329ff"
          />
          <meta
            name="google-site-verification"
            content="kBlj5s0l4ihpu0RGE1q28Q6lR2nx5VmovuxhKjUwKss"
          />

          <link
            rel="preload"
            href="https://use.typekit.net/dcw8buw.css"
            as="style"
          />
          <link rel="stylesheet" href="https://use.typekit.net/dcw8buw.css" />
          <noscript>
            <link rel="stylesheet" href="https://use.typekit.net/dcw8buw.css" />
          </noscript>
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            type="text/javascript"
            src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"
          ></script>
          <script
            type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiIesZoEvupbV94CZPR_nczMhNPoW_7c0&libraries=places"
          ></script>
          <script
            id="stripe-js"
            src="https://js.stripe.com/v3/?advancedFraudSignals=false"
            async
          ></script>
        </body>
      </Html>
    );
  }
}
