import ShowSingleJob from "../../components/show-single-job";
import { useSelector } from "react-redux";
import { Server } from "../../utils/server";
import { BASE_JOBS_API } from "../../constants/paths";

export default ({ job }) => {
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowSingleJob
      job={job}
      favourited={favourited}
      pageTitle={`${job.title} - ${job.company_name} - JustRemote`}
      pageDesc={`${job.company_name} are looking for a ${job.title}! The role is ${job.remote_type} and they're looking for applicants immediately. Find out more and apply today. No sign up required!`}
    />
  );
};

export async function getStaticPaths() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = response.data.filter((job) => job.category === "developer");

  const buildpaths = jobs.map((job) => "/" + job.href);

  return {
    paths: buildpaths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const response = await Server({
    method: "get",
    url: `${BASE_JOBS_API}/${params[":id"]}`,
  });
  const job = response.data;

  return {
    props: {
      job,
    },
  };
}
