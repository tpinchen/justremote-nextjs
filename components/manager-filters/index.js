import React from "react";
import { withRouter } from "next/router";
import { paths } from "../../constants/paths";
import { LinkChip } from "../../atoms/chip";
import { Filter } from "../../atoms/filter";
import { FilterLabel } from "../../atoms/filter-label";
import { isManagerRoute } from "../../utils/route-helpers";

export const ManagerFilters = ({ location }) => (
  <>
    {isManagerRoute(location) && (
      <Filter>
        <FilterLabel>Sub-Categories</FilterLabel>
        <LinkChip
          selected={location.pathname === paths.manager_jobs}
          label="All Manager/Exec"
          path={paths.manager_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.hr_jobs}
          label="HR"
          path={paths.hr_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.recruiter_jobs}
          label="Recruiter"
          path={paths.recruiter_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.sales_jobs}
          label="Sales"
          path={paths.sales_jobs}
        />
      </Filter>
    )}
  </>
);

export default withRouter(ManagerFilters);
