import styled from "styled-components";
import Head from "next/head";
import CompanyListings from "../../components/company-listings";
import PopularCategories from "../../components/popular-categories";
import { colors, media } from "../../constants/theme";
import { paths, BASE_COMPANIES_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const Wrapper = styled.div``;

const Heading = styled.h1`
  font-size: 40px;
  letter-spacing: -0.05em;
  margin: 0;
  line-height: 1em;
  margin-bottom: 14px;
`;

const SubHeading = styled.p`
  font-size: 18px;
  color: #b0b0b0;
  font-weight: 300;
  line-height: 23px;
  margin: 0;
`;

const HeroContent = styled.div`
  text-align: center;
  max-width: 700px;
  padding-left: 16px;
  padding-right: 16px;
`;

const HeroWrapper = styled.div`
  background-color: ${colors.backgroundBlue};
  height: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 78px;
  ${media.desktop`
    height: 350px;
  `}
`;

export const Companies = (props) => {
  return (
    <>
      <Head>
        <title>Remote Companies - JustRemote</title>
        <meta
          name="description"
          content={`Discover top companies from around the world that offer fully or partially remote positions. All job positions shown are live and can be applied for right now. Apply today and start living the life of a remote worker.`}
        />
        <link
          rel="canonical"
          href={`https://justremote.co${paths.companies}`}
        />
      </Head>
      <Wrapper>
        <HeroWrapper>
          <HeroContent>
            <Heading>Remote Companies</Heading>
            <SubHeading>
              Discover amazing companies from around the world that offer remote
              work
            </SubHeading>
          </HeroContent>
        </HeroWrapper>
        <CompanyListings companies={props.companies} />
        <PopularCategories />
      </Wrapper>
    </>
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_COMPANIES_API,
  });

  const companies = response.data;

  return {
    props: {
      companies,
    },
  };
}

export default Companies;
