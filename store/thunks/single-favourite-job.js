import { Server } from "../../utils/server";
import { BASE_JOBS_API } from "../../constants/paths";
import {
  fetchSingleFavouriteJobSuccess,
  fetchingSingleFavouriteJob,
  fetchSingleFavouriteJobError
} from "../actions/favourite-actions";

const buildRequestURL = jobId => {
  return `${BASE_JOBS_API}/${jobId}`;
};

export const fetchSingleFavouriteJob = jobId => dispatch => {
  const requestUrl = buildRequestURL(jobId);
  dispatch(fetchingSingleFavouriteJob());

  return Server({
    method: "get",
    url: requestUrl
  })
    .then(response => response.data)
    .then(job => dispatch(fetchSingleFavouriteJobSuccess(job)))
    .catch(err => {
      console.log(err);
      dispatch(fetchSingleFavouriteJobError(err));
    });
};
