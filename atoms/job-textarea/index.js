import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledJobTextArea = styled.div`
  margin-bottom: 50px;

  ${media.desktop`
    &:last-child {
      margin-bottom:0;
    }
    ${(props) =>
      props.hiddenDesktop &&
      `
      display:none;
    `}
  `}
`;

const Heading = styled.h2`
  font-size: 30px;
  margin-bottom: 8px;
  font-weight: 600;
  letter-spacing: -0.03em;
`;

const Content = styled.div`
  font-size: 16px;
  color: rgba(0, 0, 0, 0.86);
  line-height: 1.6;
  font-weight: 400;
  word-wrap: break-word;
  text-rendering: optimizeLegibility;
  ${media.tablet`
    font-size: 18px;
  `}
`;

const createMarkup = (html) => {
  return { __html: html };
};

export const JobTextArea = (props) => {
  return (
    <StyledJobTextArea hiddenDesktop={props.hiddenDesktop}>
      {props.heading && <Heading>{props.heading}</Heading>}
      <Content>
        <div dangerouslySetInnerHTML={createMarkup(props.content)} />
      </Content>
    </StyledJobTextArea>
  );
};
