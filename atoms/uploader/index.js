import React, { Component } from 'react';
import styled from 'styled-components';
import Dropzone from 'react-dropzone';

const DropZoneWrapper = styled.div`
  .uploader {
    border: 1px dashed #ddd;
    padding: 15px;
    text-align:center;
    background-color: #fff;

    &:hover {
      cursor:pointer;
    }
  }
`

const PreviewImage = styled.img`
  max-height: 100px;
  margin:0 auto 15px;
  border-radius: 3px;
`

const PreviewWrapper = styled.div`
  text-align:center;
`

export class Uploader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: []
    }

    this.readFile = this.readFile.bind(this);
  }

  readFile(file) {
    if(file) {
      // Add Base 64 encoded file to object
      let reader = new FileReader();
      reader.readAsDataURL(file[0])
      reader.onload = () => {
        file.encoded = reader.result
        this.setState({
          file
        }, () => this.props.updateState(this.state.file) )
      }
      reader.onerror = function(error) {
        console.log('Error: ', error);
      }
    }
  }

  render() {
    return(
      <DropZoneWrapper>
        <Dropzone 
          className='uploader' 
          onDrop={this.readFile}
          maxSize={this.props.maxSize}
          accept={this.props.accept}>
          { this.state.file && this.state.file[0] ? (
            <div>
              <PreviewWrapper>
                <PreviewImage src={this.state.file[0].preview} />            
              </PreviewWrapper>
              <p>Drag or click again to change image</p>
            </div>
          ) : (
            <p>{this.props.copy}</p>
          )}
        </Dropzone>
      </DropZoneWrapper>
    )
  }
}