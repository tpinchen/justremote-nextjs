export const defaultArticleState = {
  uid: '',  
  title: [],
  content: [],
  author: {},
  meta_description: '',
  social_share_image: ''
};