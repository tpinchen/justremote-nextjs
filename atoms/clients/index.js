import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 10px 24px;
`;
const ClientsWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: center;
`;

const Client = styled.img`
  width: 50px;
  margin: 0 4px;
  border-radius: 3px;
  margin-bottom: 10px;

  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
`;

const Heading = styled.p`
  margin-top: 8px;
  font-size: 14px;
  text-align: center;
  color: #bbb;
`;

export const Clients = () => {
  return (
    <Wrapper>
      <ClientsWrapper>
        <Client
          title="Aha!"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/597/logo.png"
          alt="Aha! Logo"
        />
        <Client
          title="Wildbit"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/1351/logo.png"
          alt="Wildbit Logo"
        />
        <Client
          title="Knack"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/1027/logo.png"
          alt="Knack Logo"
        />
        <Client
          title="AmazingCo"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/856/logo.png"
          alt="AmazingCo Logo"
        />
        <Client
          title="Flyt"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/659/logo.png"
          alt="Flyt Logo"
        />
        <Client
          title="Modern Tribe"
          src="https://remoteworker-live-superbrnds.s3.amazonaws.com/uploads/company/logo/346/modern_tribe.png"
          alt="Modern Tribe Logo"
        />
      </ClientsWrapper>
      <Heading>Trusted by top Remote Companies</Heading>
    </Wrapper>
  );
};
