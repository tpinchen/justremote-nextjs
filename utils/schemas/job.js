import React from "react";

const buildJobDescription = (
  about,
  experience,
  offer,
  company,
  company_name
) => {
  return `
    <h5>About:</h5>
    ${about}
    <h5>Experience:</h5>
    ${experience}
    <h5>Salary and Perks:</h5>
    ${offer}
    <h5>About ${company_name}:</h5>
    ${company}
  `;
};

const JobData = (job) => {
  return {
    "@context": "http://schema.org",
    "@type": "JobPosting",
    title: job.title,
    datePosted: job.datePosted,
    validThrough: job.validThrough,
    employmentType: job.employmentType,
    jobLocation: {
      "@type": "Place",
      address: {
        "@type": "PostalAddress",
        addressCountry: job.job_country,
      },
    },
    jobLocationType: "TELECOMMUTE",
    applicantLocationRequirements: {
      "@type": "Country",
      name: job.job_country,
    },
    hiringOrganization: {
      "@type": "Organization",
      name: job.company_name,
      sameAs: job.company_url,
      logo: job.company_logo,
    },
    description: buildJobDescription(
      job.about_role,
      job.who_looking_for,
      job.our_offer,
      job.about_job_company,
      job.company_name
    ),
  };
};

export const JobSchema = (props) => {
  return (
    <div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{ __html: JSON.stringify(JobData(props.job)) }}
      />
    </div>
  );
};
