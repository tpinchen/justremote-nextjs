import { ShowJobs } from "../../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const getDeveloperJobs = (jobs) => {
  return jobs.filter((job) => job.category === "developer");
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.developer.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.developer.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="developer"
      canonical={`https://justremote.co${paths.developer_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Developer Jobs - JustRemote"
      pageDesc="Remote Developer Jobs from around the globe. Frontend and backend developer positions including ReactJS, Javascript, Java, Python and Angular jobs."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getDeveloperJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
