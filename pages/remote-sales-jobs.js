import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getSalesJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return (
      title.indexOf("sales") >= 0 || title.indexOf("business development") >= 0
    );
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.manager.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.manager.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="sales"
      canonical={`https://justremote.co${paths.sales_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Sales Jobs - JustRemote"
      pageDesc="Remote Sales Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getSalesJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
