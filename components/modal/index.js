import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { closeModal } from "../../store/actions/modal-actions";
import { useEffect } from "react";
import { useRef } from "react";
import { media } from "../../constants/theme";

const Background = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 8;
`;

const ModalPanel = styled.div`
  position: relative;
  max-width: 500px;
  background-color: #fff;
  border-radius: 5px;
  z-index: 9;
  margin-left: 16px;
  margin-right: 16px;
  padding: 16px;
  ${media.desktop`
    padding: 50px;
  `}
`;

const ExperimentHeading = styled.h3`
  font-size: 16px;
  text-align: center;
  ${media.desktop`
    font-size: 20px;
  `};
`;

const CloseModalLink = styled.p`
  text-align: center;
  text-decoration: underline;
  margin-top: 24px;

  ${media.desktop`
    margin-bottom: 0;
  `}

  &:hover {
    cursor: pointer;
  }
`;

const ResumeInner = (props) => {
  return (
    <div>
      <ExperimentHeading>🔥 Awesome Resumes Coming Soon 🔥</ExperimentHeading>
      <p>
        At JustRemote we're all about making it as easy as possible for you to
        get a remote job and be able to live a flexible life.
      </p>
      <p>As part of this we want to help you create amazing resumes.</p>
      <p>
        The kind of resumes that stand out from the crowd, get noticed and help
        you reach the interview stage.
      </p>
      <p>
        Unfortunately this feature isn't quite ready yet but keep checking back
        and it will be here soon.
      </p>
      <CloseModalLink onClick={props.closeModal}>
        Okay, got it! Close message
      </CloseModalLink>
    </div>
  );
};

export const Modal = (props) => {
  const ref = useRef();

  const handleClick = (e) => {
    if (props.isOpen && ref.current && !ref.current.contains(e.target)) {
      props.closeModal();
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClick);
    return function cleanup() {
      document.removeEventListener("click", handleClick);
    };
  });

  if (props.isOpen) {
    return (
      <Background>
        <ModalPanel ref={ref}>
          <ResumeInner closeModal={props.closeModal} />
        </ModalPanel>
      </Background>
    );
  }
  return null;
};

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(closeModal()),
});

const mapStateToProps = (state) => ({
  isOpen: state.modalState.open,
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
