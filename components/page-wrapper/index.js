import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledPageWrapper = styled.div`
  margin-top: 57px;
  min-height: calc(100vh - 139px);
  background-color: ${(props) => (props.bgColor ? props.bgColor : "")};

  ${media.desktop`
    margin-top:78px;    
  `}
`;

export const PageWrapper = (props) => {
  return (
    <StyledPageWrapper bgColor={props.bgColor}>
      {props.children}
    </StyledPageWrapper>
  );
};
