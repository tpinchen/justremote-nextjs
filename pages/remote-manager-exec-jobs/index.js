import { ShowJobs } from "../../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.manager.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.manager.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="manager"
      canonical={`https://justremote.co${paths.manager_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Business/Manager Jobs - JustRemote"
      pageDesc="Remote Manager Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = response.data.filter((job) => job.category === "manager");

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
