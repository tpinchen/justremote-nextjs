import React from "react";
import styled from "styled-components";

const InactiveJobWrapper = styled.div`
  max-width: 700px;
  margin: 30px auto;
`;

const Hero = styled.div`
  min-height: 276px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Image = styled.img`
  max-width: 125px;
`;

export const JobInactive = props => {
  const { title, company_name, company_logo } = props.job;
  return (
    <InactiveJobWrapper>
      <Hero>
        {company_logo.url && company_logo.url.length ? (
          <Image src={company_logo.url} alt={company_name} />
        ) : null}
        <h1>{title}</h1>
        <p>{company_name}</p>
        <p>Unfortuntely this position is no longer available.</p>
      </Hero>
    </InactiveJobWrapper>
  );
};
