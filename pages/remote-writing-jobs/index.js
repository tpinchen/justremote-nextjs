import { ShowJobs } from "../../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.writing.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.writing.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="writing"
      canonical={`https://justremote.co${paths.writing_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Writing Jobs - JustRemote"
      pageDesc="Remote Writing Jobs from around the globe. Freelance and full time writing jobs for copywriters, content writers and more."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = response.data.filter((job) => job.category === "writing");

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
