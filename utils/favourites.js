export const isFavourited = (jobId, favourited) => {
  if (favourited && favourited.length > 0) {
    return !!favourited.find((favourite) => favourite.id === jobId);
  } else {
    return false;
  }
};
