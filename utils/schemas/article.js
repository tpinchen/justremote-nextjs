import React from "react";

const articleData = (article) => {
  return {
    "@context": "http://schema.org",
    "@type": "BlogPosting",
    mainEntityOfPage: {
      "@type": "WebPage",
      "@id": `https://justremote.co/articles/${article.url ? article.url : ""}`,
    },
    headline: `${article.headline ? article.headline : ""}`,
    image: [article.image],
    datePublished: article.published_date ? article.published_date : "",
    dateModified: article.modified_date ? article.modified_date : "",
    author: {
      "@type": "Person",
      name: article.author.length ? article.author[0].text : "",
    },
    publisher: {
      "@type": "Organization",
      name: "JustRemote",
      logo: {
        "@type": "ImageObject",
        url:
          "https://prismic-io.s3.amazonaws.com/justremote%2F90fbf64f-afb3-4476-977b-97b8bd3483db_icon.jpg",
      },
    },
    description: article.description ? article.description : "",
  };
};

export const ArticleSchema = (props) => {
  return (
    <div>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(articleData(props.article)),
        }}
      />
    </div>
  );
};
