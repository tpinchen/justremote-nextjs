import React from "react";
import { withRouter } from "next/router";
import { paths } from "../../constants/paths";
import { LinkChip } from "../../atoms/chip";
import { Filter } from "../../atoms/filter";
import { FilterLabel } from "../../atoms/filter-label";
import { isWritingRoute } from "../../utils/route-helpers";

export const WritingFilters = ({ location }) => (
  <>
    {isWritingRoute(location) && (
      <Filter>
        <FilterLabel>Sub-Categories</FilterLabel>
        <LinkChip
          selected={location.pathname === paths.writing_jobs}
          label="All Writing"
          path={paths.writing_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.copywriter_jobs}
          label="Copywriter"
          path={paths.copywriter_jobs}
        />
        <LinkChip
          selected={location.pathname === paths.editing_jobs}
          label="Editing"
          path={paths.editing_jobs}
        />
      </Filter>
    )}
  </>
);

export default withRouter(WritingFilters);
