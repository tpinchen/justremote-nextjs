import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const Index = (props) => {
  const filters = useSelector((state) => state.filterState.all.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.all.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category=""
      canonical={`https://justremote.co`}
      jobs={props.jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Jobs: Programming, Design, Marketing, Writing & more"
      pageDesc="Remote jobs for professional remote workers. Start working remotely today and telecommute anywhere around the world. Discover developer, designer, marketing job opportunities from the world's best remote companies."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = response.data;

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
