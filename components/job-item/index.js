import React, { Component } from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";
import { withRouter } from "next/router";
import { TrackClick } from "../../utils/analytics";
import { HeartFilled, HeartEmpty } from "../../atoms/favourite-button";

const JobItemWrapper = styled.div`
  padding: 0 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: #fff;
  margin-bottom: 15px;
  box-shadow: 0 0 4px 0px rgba(0, 0, 0, 0.15);
  border-radius: 3px;
  border-left: 3px solid transparent;
  transition: 0.1s ease-in;

  ${media.desktop`
    border-left: 3px solid ${(props) =>
      props.selected ? "hsla(163,100%,50%,1)" : "transparent"};
    
    &:hover {
      border-left: 3px solid ${(props) =>
        props.selected ? "hsla(163,100%,50%,1)" : "hsla(163,100%,85%,1)"};
    }
  `}
`;
const JobItemInnerMeta = styled.div`
  margin-left: 15px;
  margin-right: 10px;
`;
const JobItemInnerWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
`;
const JobItemCompany = styled.div`
  display: inline-block;
  margin-right: 10px;
`;
const JobItemType = styled.div`
  display: inline-block;
  font-style: italic;
  font-size: 12px;
  text-transform: capitalize;
  letter-spacing: 0.05em;
`;
const JobItemDate = styled.div`
  font-size: 14px;
  font-weight: 400;
  white-space: nowrap;
`;
const HeartIcon = styled.div`
  cursor: pointer;
  display: block;

  &:hover {
    cursor: pointer;
  }
`;
const JobMeta = styled.a`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  padding: 15px 0;

  &:hover {
    cursor: pointer;
  }
`;
const JobTitle = styled.h4`
  overflow: hidden;
  text-overflow: ellipsis;
  word-break: break-all;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`;

export class JobItem extends Component {
  handleFavouriteClick = () => {
    this.props.handleFavouriteClick(this.props.job);
  };

  handleJobClick = (e) => {
    e.preventDefault();

    const { router, job } = this.props;

    TrackClick({
      category: "Job Item",
      action: `Link Clicked - ${job.category}`,
      label: job.href,
    });

    router.push(`/${job.href}`);
  };

  render() {
    return (
      <JobItemWrapper selected={this.props.selected}>
        <JobItemInnerWrap>
          <HeartIcon>
            {this.props.favourited ? (
              <HeartFilled onClick={this.handleFavouriteClick} height="20" />
            ) : (
              <HeartEmpty onClick={this.handleFavouriteClick} height="20" />
            )}
          </HeartIcon>
          <JobMeta href={this.props.job.href} onClick={this.handleJobClick}>
            <JobItemInnerMeta>
              <JobTitle>{this.props.job.title}</JobTitle>
              <JobItemCompany>{this.props.job.company_name}</JobItemCompany>
              <JobItemType>{this.props.job.remote_type}</JobItemType>
            </JobItemInnerMeta>
            <JobItemDate>{this.props.job.date}</JobItemDate>
          </JobMeta>
        </JobItemInnerWrap>
      </JobItemWrapper>
    );
  }
}

export default withRouter(JobItem);
