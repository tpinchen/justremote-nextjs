import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledFilterButton = styled.li`
  list-style: none;
  font-size: 13px;
  padding-top: 15px;
  padding-bottom: 15px;
  margin-right: 0px;
  font-weight: 400;
  letter-spacing: 1px;
  user-select: none;
  position: relative;
  justify-content: space-around;

  &:hover {
    cursor: pointer;
  }

  ${media.tablet`
    margin-right: 50px;
  `};
`;

const Indicator = styled.div`
  height: 4px;
  width: 4px;
  background-color: #ff6666;
  position: absolute;
  left: -10px;
  top: 21px;
  border-radius: 4px;
`;

export const FilterButton = (props) => {
  const { title, handleClick, filterApplied } = props;
  return (
    <StyledFilterButton onClick={handleClick}>
      {title}

      {filterApplied ? <Indicator /> : null}
    </StyledFilterButton>
  );
};
