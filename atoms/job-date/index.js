import React from 'react';
import styled from 'styled-components';


const StyledJobDate = styled.p`
  text-transform:uppercase;
  font-size:12px;
  color: #AAA;
  text-align:center;
  letter-spacing: 1px;
`

export const JobDate = props => {
  return (
    <StyledJobDate>{props.date}</StyledJobDate>
  )
}