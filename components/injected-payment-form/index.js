import React, { Component } from "react";
import styled from "styled-components";
import { injectStripe, CardElement } from "react-stripe-elements";
import { Prices } from "../../constants/prices";
import { newJobFormValidation } from "../../form-validations/new-job-form-validation";
import { Button } from "../../atoms/button";
import { Label } from "../../atoms/label";

const CardElementWrapper = styled.div`
  margin-bottom: 30px;
  margin-top: 5px;
  border: 1px solid #ddd;
  padding: 15px;
  background-color: #fff;
  border-radius: 3px;
`;

const getURLParam = (variable) => {
  const query = window.location.search.substring(1);
  let vars = query.split("&");
  for (let i = 0; i < vars.length; i++) {
    let pair = vars[i].split("=");
    if (pair[0] === variable) return pair[1];
  }
  return null;
};

class PaymentForm extends Component {
  constructor(props) {
    super(props);
    this.validator = newJobFormValidation;
  }

  handleClick = (ev) => {
    ev.preventDefault();
    const { formState, pushLocation, setValidation, submitNewJob } = this.props;
    const validate = this.validator.validate(formState);

    const fullname = `${formState.contact_first_name} ${formState.contact_surname}`;

    // Validate all fields
    if (validate.isValid) {
      if (!!formState.rwa) {
        submitNewJob(formState, pushLocation, null, 0);
      } else {
        let provider = getURLParam("provider");
        // Request Stripe Payment Token
        this.props.stripe.createToken({ name: fullname }).then(({ token }) => {
          if (token) submitNewJob(formState, pushLocation, token, provider);
        });
      }
    } else {
      setValidation(this.validator.validate(formState));
    }
  };

  render() {
    return (
      <form>
        <Label
          title={"Card Details*"}
          tooltipTitle="Payment is securely powered by Stripe"
        />
        <CardElementWrapper>
          <CardElement />
        </CardElementWrapper>
        <Button
          title={`Post job - $${Prices.single_job}/30 days`}
          path="/"
          fullWidth={true}
          align={"center"}
          click={this.handleClick}
        />
      </form>
    );
  }
}

export const InjectedPaymentForm = injectStripe(PaymentForm);
