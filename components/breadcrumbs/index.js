import Link from "next/link";
import { withRouter } from "next/router";
import styled from "styled-components";
import { paths } from "../../constants/paths";

const StyledLink = styled.a`
  display: block;
  text-align: center;
  margin-top: 16px;
  font-size: 14px;
  color: blue;
  letter-spacing: 1px;
`;

export const Breadcrumbs = (props) => {
  const setBreadcrumbPath = () => {
    const { category } = props;
    if (category === "developer") {
      return paths.developer_jobs;
    }
    if (category === "design") {
      return paths.design_jobs;
    }
    if (category === "marketing") {
      return paths.marketing_jobs;
    }
    if (category === "manager") {
      return paths.manager_jobs;
    }
    if (category === "customerservice") {
      return paths.customer_service_jobs;
    }
    if (category === "writing") {
      return paths.writing_jobs;
    }
  };

  const setBreadcrumbCategory = () => {
    const { category } = props;
    if (category === "manager") {
      return "manager/exec";
    }
    if (category === "customerservice") {
      return "customer service";
    }
    return category;
  };

  if (props.router.pathname !== paths.post_job) {
    return (
      <Link href={setBreadcrumbPath()} passHref>
        <StyledLink>View all {setBreadcrumbCategory()} jobs</StyledLink>
      </Link>
    );
  }
  return null;
};

export default withRouter(Breadcrumbs);
