import { Component } from "react";
import Head from "next/head";
import styled from "styled-components";
import { withRouter } from "next/router";
import { media } from "../../constants/theme";
import { PageWrapper } from "../../components/page-wrapper";
import { Inner } from "../../atoms/inner";
import { RichText } from "prismic-reactjs";
import { fetchArticle } from "../../store/thunks/article";
import { ArticleSchema } from "../../utils/schemas/article";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const ContentWrap = styled.article`
  max-width: 700px;
  margin: 30px auto 100px;

  h1 {
    margin-bottom: 30px;
    font-size: 32px;
    letter-spacing: -0.015em;
    color: rgba(0, 0, 0, 0.86);
    line-height: 1.04;
  }

  h2,
  h3 {
    margin-top: 30px;
    margin-bottom: 10px;
  }

  p,
  li {
    margin-top: 0;
    font-size: 20px;
    color: rgba(0, 0, 0, 0.86);
    line-height: 1.5em;
    text-rendering: optimizeLegibility;
    font-family: "baskerville, serif";
  }

  ul {
    margin-left: 40px;
  }

  a {
    text-decoration: underline;
  }

  ${media.desktop`
    h1 {
      font-size: 36px;
    }
    p {
      font-size: 22px;
    }
  
  `}
`;

const Author = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  border-top: 1px solid #f4f6f7;
  padding-top: 15px;

  img {
    border-radius: 100%;
    max-width: 80px;
    margin-right: 20px;
  }
`;

const AuthorMeta = styled.div`
  p {
    margin: 0;
    font-size: 16px;
  }
`;

const ArticleContent = styled.div`
  margin-bottom: 60px;
`;
const AuthorSocialLinks = styled.div`
  display: inline-flex;
  a {
    margin-right: 5px;
  }
`;

const getArticleId = (url) => {
  return url.split("/")[2]; // Return the second part of the URL
};

export class Article extends Component {
  shouldComponentUpdate(props) {
    let url = window.location.pathname;
    if (props.uid !== getArticleId(url)) {
      window.scrollTo(0, 0);
      this.props.fetchArticle(getArticleId(url));
    }
    return true;
  }

  componentDidMount() {
    let url = window.location.pathname;
    if (this.props.uid !== getArticleId(url)) {
      window.scrollTo(0, 0);
      this.props.fetchArticle(getArticleId(url));
    }
    if (
      window.addthis &&
      window.addthis.layers &&
      window.addthis.layers.initialized
    ) {
      window.addthis.layers.refresh();
    }
  }

  componentDidUpdate() {
    if (
      window.addthis &&
      window.addthis.layers &&
      window.addthis.layers.initialized
    ) {
      let newURL = `https://justremote.co${this.props.router.pathname}`;
      window.addthis.update("share", "url", newURL);
      window.addthis.update("share", "title", this.props.title[0].text);
    }
  }

  render() {
    const {
      title,
      content,
      author,
      meta_description,
      social_share_image,
      uid,
      date_published,
      date_modified,
    } = this.props;
    return (
      <div>
        <Head>
          <title>{title.length > 0 ? title[0].text : ""} - JustRemote</title>
          <meta
            name="description"
            content={meta_description ? meta_description : ""}
          />
          <meta
            property="og:title"
            content={title.length > 0 ? title[0].text : ""}
          />
          <meta property="og:type" content="article" />
          <meta property="og:image" content={social_share_image} />

          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@justremoteco" />
          <meta name="twitter:creator" content="@supert56" />
          <meta
            name="twitter:title"
            content={title.length > 0 ? title[0].text : ""}
          />
          <meta
            name="twitter:description"
            content={meta_description ? meta_description : ""}
          />
          <meta name="twitter:image" content={social_share_image} />

          <script
            type="text/javascript"
            src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b619175f4a5f3f9"
            defer
          ></script>
        </Head>
        <PageWrapper>
          <ArticleSchema
            article={{
              url: uid ? uid : "",
              headline: title.length ? title[0].text : "",
              published_date: date_published ? date_published : "",
              modified_date: date_modified ? date_modified : "",
              image: social_share_image ? social_share_image : "",
              author: author.data ? author.data.name : "",
              description: meta_description ? meta_description : "",
            }}
          />
          <Inner>
            <ContentWrap>
              {title.length > 0 ? <div>{RichText.render(title)}</div> : null}
              {content.length > 0 ? (
                <ArticleContent>{RichText.render(content)}</ArticleContent>
              ) : null}
              <p>Share this article:</p>
              <div className="addthis_inline_share_toolbox"></div>
              <br />
              {Object.keys(author).length > 0 ? (
                <Author>
                  <img
                    src={author.data.avatar.url}
                    alt={`${author.data.name.text}`}
                  />
                  <AuthorMeta>
                    {RichText.render(author.data.name)}
                    {RichText.render(author.data.position_and_website)}
                    <AuthorSocialLinks>
                      <a
                        href={`https://www.twitter.com/${author.data.twitter_handle[0].text}`}
                        target="_blank"
                      >
                        <svg width="29" height="29" viewBox="0 0 29 29">
                          <path d="M22.053 7.54a4.474 4.474 0 0 0-3.31-1.455 4.526 4.526 0 0 0-4.526 4.524c0 .35.04.7.082 1.05a12.9 12.9 0 0 1-9.3-4.77c-.39.69-.61 1.46-.65 2.26.03 1.6.83 2.99 2.02 3.79-.72-.02-1.41-.22-2.02-.57-.01.02-.01.04 0 .08-.01 2.17 1.55 4 3.63 4.44-.39.08-.79.13-1.21.16-.28-.03-.57-.05-.81-.08.54 1.77 2.21 3.08 4.2 3.15a9.564 9.564 0 0 1-5.66 1.94c-.34-.03-.7-.06-1.05-.08 2 1.27 4.38 2.02 6.94 2.02 8.31 0 12.86-6.9 12.84-12.85.02-.24.01-.43 0-.65.89-.62 1.65-1.42 2.26-2.34-.82.38-1.69.62-2.59.72a4.37 4.37 0 0 0 1.94-2.51c-.84.53-1.81.9-2.83 1.13z"></path>
                        </svg>
                      </a>
                      <a
                        href={`https://www.medium.com/@${author.data.medium_handle[0].text}`}
                        target="_blank"
                      >
                        <svg width="29" height="29" viewBox="0 0 50 50">
                          <path d="M 18 41.578125 C 18 42.46875 17.523438 43 16.847656 43 C 16.609375 43 16.34375 42.933594 16.0625 42.792969 L 5.164063 37.273438 C 4.523438 36.949219 4 36.089844 4 35.367188 L 4 8.3125 C 4 7.597656 4.378906 7.175781 4.921875 7.175781 C 5.113281 7.175781 5.328125 7.226563 5.550781 7.339844 L 5.933594 7.535156 C 5.933594 7.535156 5.9375 7.535156 5.9375 7.535156 L 17.949219 13.617188 C 17.96875 13.628906 17.984375 13.648438 18 13.664063 Z M 30.585938 8.882813 L 31.320313 7.675781 C 31.582031 7.246094 32.0625 7 32.550781 7 C 32.664063 7 32.78125 7.015625 32.894531 7.046875 C 32.980469 7.066406 33.070313 7.097656 33.164063 7.144531 L 45.832031 13.558594 C 45.835938 13.558594 45.835938 13.5625 45.835938 13.5625 L 45.847656 13.566406 C 45.855469 13.570313 45.855469 13.582031 45.863281 13.585938 C 45.925781 13.636719 45.953125 13.722656 45.910156 13.792969 L 33.292969 34.558594 L 32.003906 36.675781 L 23.644531 20.304688 Z M 20 30.605469 L 20 17.5625 L 28.980469 35.15625 L 20.902344 31.066406 Z M 46 41.578125 C 46 42.414063 45.503906 42.890625 44.773438 42.890625 C 44.445313 42.890625 44.070313 42.796875 43.675781 42.597656 L 41.867188 41.679688 L 33.789063 37.589844 L 46 17.496094 Z "></path>
                        </svg>
                      </a>
                    </AuthorSocialLinks>
                  </AuthorMeta>
                </Author>
              ) : null}
            </ContentWrap>
          </Inner>
        </PageWrapper>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    uid: state.articleState.uid,
    title: state.articleState.title,
    content: state.articleState.content,
    author: state.articleState.author,
    meta_description: state.articleState.meta_description,
    social_share_image: state.articleState.social_share_image,
    date_published: state.articleState.date_published,
    date_modified: state.articleState.date_modified,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticle: bindActionCreators(fetchArticle, dispatch),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Article)
);
