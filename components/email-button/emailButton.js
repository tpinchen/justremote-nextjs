import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";
import { withRouter } from "next/router";
import { paths } from "../../constants/paths";

const EnvelopeWrapper = styled.div`
  margin-right: 15px;
  display:flex;
  background-color: transparent;
  color: #fff;
  padding: 10px 15px;
  border-radius: 3px;
  font-weight: 400;
  letter-spacing:1px;
  font-size: 13px;

  i {
    font-size: 18px;
    color: #1b1b1b;
    }
  }

  &:hover {
    cursor: pointer;
  }

  
  @media (min-width: 1200px) {
    
    background-color: #1b1b1b;
    i {
      color: #fff !important;
      margin-right: 10px;
    }
  }

`;
const Text = styled.span`
  display: none;

  @media (min-width: 1200px) {
    display: block;
    margin-left: 8px;
  }
`;

const StyledPath = styled.path`
  fill: #1b1b1b;
  ${media.desktop`
    fill: ${(props) => (props.isHome ? "#fff" : "#1b1b1b")};
  `}

  @media (min-width: 1200px) {
    fill: #fff;
  }
`;

export const EmailButton = (props) => {
  const handleClick = () => {
    if (props.open) {
      props.hideEmailForm();
    } else {
      props.showEmailForm();
    }
  };

  return (
    <EnvelopeWrapper onClick={handleClick}>
      <svg
        enableBackground="new 0 0 479.058 479.058"
        height="16"
        viewBox="0 0 479.058 479.058"
        width="16"
        xmlns="http://www.w3.org/2000/svg"
      >
        <StyledPath
          isHome={props.location.pathname === paths.home}
          d="m434.146 59.882h-389.234c-24.766 0-44.912 20.146-44.912 44.912v269.47c0 24.766 20.146 44.912 44.912 44.912h389.234c24.766 0 44.912-20.146 44.912-44.912v-269.47c0-24.766-20.146-44.912-44.912-44.912zm0 29.941c2.034 0 3.969.422 5.738 1.159l-200.355 173.649-200.356-173.649c1.769-.736 3.704-1.159 5.738-1.159zm0 299.411h-389.234c-8.26 0-14.971-6.71-14.971-14.971v-251.648l199.778 173.141c2.822 2.441 6.316 3.655 9.81 3.655s6.988-1.213 9.81-3.655l199.778-173.141v251.649c-.001 8.26-6.711 14.97-14.971 14.97z"
        />
      </svg>
      <Text>Job Alerts</Text>
    </EnvelopeWrapper>
  );
};

export default withRouter(EmailButton);
