import { ShowJobs } from "../../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const getCustomerServiceJobs = (jobs) => {
  return jobs.filter((job) => job.category === "customerservice");
};

const Index = ({ jobs }) => {
  const favourited = useSelector((state) => state.favouriteState.favourites);
  const filters = useSelector(
    (state) => state.filterState.customerservice.filters
  );
  const filtersApplied = useSelector(
    (state) => state.filterState.developer.filtersApplied
  );

  return (
    <ShowJobs
      category="customerservice"
      canonical={`https://justremote.co${paths.customer_service_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Customer Service Jobs - JustRemote"
      pageDesc="Remote Customer Service Jobs now hiring on JustRemote.co and start working remotely today."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getCustomerServiceJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
