import Link from "next/link";
import styled from "styled-components";
import NewJobItem from "../new-job-item";
import Filtering from "../filtering";
// import { Partner } from "../partner";
import { colors, maxInnerWrapperWidth, media } from "../../constants/theme";
import { connect } from "react-redux";
import { paths } from "../../constants/paths";
import { isFavourited } from "../../utils/favourites";
import { setFavourite } from "../../store/actions";
import { Prices } from "../../constants/prices";
import { JobCategories } from "../job-categories";
import { TextNotice } from "../../atoms/text-notice";
import LazyLoad from "react-lazyload";

const Wrapper = styled.div`
  background-color: ${colors.backgroundBlue};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  ${media.desktop`
    padding-left: 32px;
    padding-right: 32px;    
  `}
`;

const Panel = styled.div`
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.15);
  border-radius: 6px;
  background-color: ${colors.white};
  max-width: ${maxInnerWrapperWidth};
  margin: 0 auto;
  width: 100%;
  position: relative;
  top: -80px;
`;
const Left = styled.div`
  display: none;
  ${media.desktop`
    display:block;
    margin-right: 100px;
    width: 30%;
    margin-bottom: 0;
    text-align:left;
  `}
`;
const Right = styled.div`
  width: 100%;
  ${media.desktop`
  width: 70%;
`}
`;

const UnderlinedLink = styled.a`
  display: block;
  color: ${colors.linkBlue};
  font-size: 18px;
  font-weight: 600;
  margin: 0px 40px 80px;
  letter-spacing: -0.04em;
  text-align: center;
`;

const Flex = styled.div`
  width: 100%;
  padding: 16px 16px 16px;
  display: flex;
  flex-direction: column;
  ${media.desktop`
    padding: 16px 24px 16px;
    flex-direction: row;
    padding: 60px;
  `}
`;

const JobCategoriesWrapper = styled.div`
  display: block;
  padding: 16px 8px;
  border-bottom: 1px solid #f4f6f7;
`;

const FeaturedJobs = styled.div`
  margin-bottom: 40px;
  ${media.desktop`
    margin-bottom: 80px;
  `}
`;

const Title = styled.h2`
  margin-top: 16px;
  margin-bottom: 16px;
  font-size: 16px;
  font-weight: 400;
  display: inline-block;
  padding-bottom: 2px;
  border-bottom: 2px solid ${colors.primaryRed};

  ${media.desktop`
    margin-top: 0;
    margin-bottom: 16px;
    text-align:left;
  `}
`;

export const JobListings = ({
  handleFavouriteClick,
  favourited,
  category,
  filters,
  filtersApplied,
  jobs,
  filteredJobs,
  featuredJobs,
}) => (
  <Wrapper>
    <Panel>
      <JobCategoriesWrapper>
        <JobCategories small />
      </JobCategoriesWrapper>
      <Flex>
        <Left>
          <Filtering
            category={category}
            filters={filters}
            filtersApplied={filtersApplied}
            jobs={jobs}
          />
          {/* <Partner /> */}
        </Left>
        <Right>
          {featuredJobs.length > 0 && (
            <>
              <FeaturedJobs>
                <Title>Featured</Title>
                {featuredJobs.map((job, i) => (
                  <NewJobItem
                    favourited={isFavourited(job.id, favourited)}
                    handleFavouriteClick={handleFavouriteClick}
                    key={i}
                    job={job}
                  />
                ))}
              </FeaturedJobs>
              <Title>All Listings</Title>
            </>
          )}

          {filteredJobs.length > 0 ? (
            filteredJobs.map((job, i) => (
              <LazyLoad height={140} offset={1000} key={i} once>
                <NewJobItem
                  favourited={isFavourited(job.id, favourited)}
                  handleFavouriteClick={handleFavouriteClick}
                  key={i}
                  job={job}
                />
              </LazyLoad>
            ))
          ) : (
            <TextNotice hasFrame>No positions currently available.</TextNotice>
          )}
        </Right>
      </Flex>
    </Panel>
    <Link href={paths.post_job} passHref>
      <UnderlinedLink>
        List your Remote Job Position for ${Prices.single_job}/30 days
      </UnderlinedLink>
    </Link>
  </Wrapper>
);

const mapDispatchToProps = (dispatch) => ({
  handleFavouriteClick(job) {
    dispatch(setFavourite(job));
  },
});

const mapStateToProps = (state) => {
  return {
    favourited: state.favouriteState.favourites,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(JobListings);
