import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledPageDivider = styled.div`
  width: ${(props) => (props.width ? props.width.mobile : null)};
  display: ${(props) => (props.display ? props.display.mobile : null)};
  background-color: ${(props) => (props.bgColor ? props.bgColor : null)};
  background-image: ${(props) =>
    props.bgImage ? `url('${props.bgImage}')` : null};
  position: ${(props) => (props.position ? props.position.mobile : null)};
  z-index: 0;

  ${media.desktop`
    width: ${(props) => (props.width ? props.width.desktop : null)};
    height: ${(props) => (props.height ? props.height : null)};
    box-shadow: ${(props) => (props.boxShadow ? props.boxShadow : null)};
    overflow-y: ${(props) => (props.overflow ? props.overflow : null)};
    display: ${(props) => (props.display ? props.display.desktop : null)};
    margin: ${(props) => (props.margin ? props.margin : null)};
    left: ${(props) => (props.left ? props.left : null)};
    position: ${(props) => (props.position ? props.position.desktop : null)};
  `}
`;

export const PageDivider = (props) => {
  return (
    <StyledPageDivider
      bgImage={props.bgImage}
      left={props.left}
      position={props.position}
      width={props.width}
      display={props.display}
      bgColor={props.bgColor}
      height={props.height}
      boxShadow={props.boxShadow}
      overflow={props.overflow}
      margin={props.margin}
    >
      {props.children}
    </StyledPageDivider>
  );
};
