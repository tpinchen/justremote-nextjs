import React from "react";
import styled from "styled-components";
import Breadcrumbs from "../breadcrumbs";
import Link from "next/link";
import { withRouter } from "next/router";
import { TechnologyAndTools } from "../../components/technology-and-tools";
import { media } from "../../constants/theme";
import { JobSchema } from "../../utils/schemas/job";
import { JobMeta } from "../../atoms/job-meta";
import { JobTitle } from "../../atoms/job-title";
import { Button } from "../../atoms/button";
import { CompanyLogo } from "../../atoms/company-logo";
import { JobRestrictions } from "../../atoms/job-restrictions";
import { JobTextArea } from "../../atoms/job-textarea";
import { paths } from "../../constants/paths";
import { TrackClick } from "../../utils/analytics";
import { JobListingsAdDesktop } from "../ads/job-listings-desktop-ad";

const JobHero = styled.div`
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
  padding: 24px;
  ${media.desktop`
    padding: 0;
  `}
`;

const JobWrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${media.desktop`
    flex-direction: row;
    max-width: 1200px;
    margin:0 auto;
  `}
`;

const JobContents = styled.div`
  width: 100%;
  display: block;
  margin: 0;
  ${media.desktop`
    width: 65%;
    margin: 60px 40px;
  
  `}
`;
const JobApply = styled.div`
  width: 100%;
  margin: 0;
  ${media.desktop`    
    margin: 80px 40px;
    width: 35%;
  `}
`;

const PaddingWrapper = styled.div`
  padding: 0 24px;
  ${media.desktop`
    padding: 0;
  `}
`;

const InnerApply = styled.div`
  position: relative;
  padding: 0 24px;
  ${media.desktop`
    position: sticky;
    top: 80px;
    padding: 0;    

    p {
      margin-top:0;
    }
  `}
`;

const CompanyDetails = styled.div`
  display: none;
  ${media.desktop`
  
  display: flex;
  align-items: center;
  margin-bottom: 16px;
  `}
`;
const MobileCompanyDetails = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 16px;
  ${media.desktop`
    display: none;
  `}
`;
const CompanyName = styled.a`
  margin-left: 0;
  margin-top: 8px;

  ${media.desktop`
    margin-top: 0;
    margin-bottom: 0;
    margin-left: 16px;
  `}
`;

const CompanyLogoPlaceholder = styled.div`
  display: block;
  width: 60px;
  height: 60px;
  background-color: #f4f6f7;
  border-radius: 3px;
`;

const CompanyAbout = styled.div`
  display: none;
  ${media.desktop`
    display:block;
    p {
      font-size: 14px;
      letter-spacing: -0.02em;
    }
  `}
`;

const AdWrapper = styled.div`
  display: none;
  ${media.desktop`
      display:block;
      max-width: 294px;    
      margin: 40px auto 0;
  `}
`;

export const NewJobPreviewDetails = (props) => {
  const {
    company_type,
    remote_type,
    about_job_company,
    job_country,
    job_formatted_location,
    company_logo,
    title,
    company_name,
    company_url,
    job_type,
    specific_country_required,
    country_list,
    overlap_required,
    overlap_hours,
    about_role,
    who_looking_for,
    our_offer,
    technology_list,
    tool_list,
    apply,
    remote_days_count,
    raw_date,
    valid_through,
    company_slug,
    category,
  } = props.job;

  const setLogo = (logo) => {
    if (Array.isArray(logo)) return logo.length > 0 ? logo[0].preview : "";
    return logo.url && logo.url.length > 0 ? logo.url : "";
  };

  const setApplyArea = () => {
    const hrefs = apply.match(/href="([^"]*)/);
    if (hrefs) {
      const link = hrefs[1];
      if (link) {
        return (
          <Button
            title="Quick Apply"
            path={link}
            click={(e) => {
              e.preventDefault();
              TrackClick({
                category: `Apply - ${category}`,
                action: `Apply Clicked - ${company_name}`,
                label: title,
              });
              window.open(link);
            }}
            fullWidth
            align={"center"}
            isGreen
          />
        );
      }
      return <JobTextArea content={apply ? apply : "apply here..."} />;
    }

    return <JobTextArea content={apply ? apply : ""} />;
  };

  return (
    <>
      {props.schema !== false && (
        <JobSchema
          job={{
            title: title,
            datePosted: raw_date,
            validThrough: valid_through,
            employmentType:
              job_type === "permanent" ? "FULL_TIME" : "CONTRACTOR",
            company_name: company_name,
            company_url: company_url,
            company_logo:
              company_logo && company_logo.url ? company_logo.url : "",
            about_role: about_role,
            who_looking_for: who_looking_for,
            our_offer: our_offer,
            about_job_company: about_job_company,
            job_country: job_country,
          }}
        />
      )}
      <JobWrapper>
        <JobContents>
          <JobHero>
            <MobileCompanyDetails>
              <CompanyLogo
                src={setLogo(company_logo)}
                alt={`${company_name} logo`}
              />

              <Link href={`${paths.companies}/${company_slug}`} passHref>
                <CompanyName>
                  {company_name ? company_name : "Company Name"}
                </CompanyName>
              </Link>
            </MobileCompanyDetails>
            <JobTitle title={title ? title : "Job Title"} />
            <JobMeta
              job_type={job_type ? job_type : "Job Type"}
              remote_type={remote_type ? remote_type : "Fully/Partially Remote"}
            />
          </JobHero>
          <JobRestrictions
            requires_specific_country={specific_country_required}
            country_list={
              country_list && country_list.length > 0 ? country_list : ""
            }
            requires_time_overlap={overlap_required}
            overlap_hours={overlap_hours}
            company_name={
              company_type === "Recruiter"
                ? "the company"
                : company_name
                ? company_name
                : "Company Name"
            }
            location={job_formatted_location}
            remote_type={remote_type}
            remote_days_count={remote_days_count}
          />
          <PaddingWrapper>
            <JobTextArea
              content={about_role ? about_role : "About your role..."}
            />
            <JobTextArea
              heading={`Experience`}
              content={
                who_looking_for
                  ? who_looking_for
                  : "Required skills and experience..."
              }
            />
            <JobTextArea heading={"Salary and Perks"} content={our_offer} />

            <TechnologyAndTools
              technologies={technology_list}
              tools={tool_list}
            />

            <JobTextArea
              hiddenDesktop
              heading={`About ${
                company_type === "Inhouse"
                  ? company_name
                    ? company_name
                    : "[Company Name]"
                  : "the Company"
              }`}
              content={
                about_job_company ? about_job_company : "About the company..."
              }
            />
          </PaddingWrapper>
        </JobContents>

        <JobApply>
          <InnerApply>
            <CompanyDetails>
              {setLogo(company_logo).length > 0 ? (
                <CompanyLogo
                  src={setLogo(company_logo)}
                  alt={`${company_name} logo`}
                />
              ) : (
                <CompanyLogoPlaceholder />
              )}
              <CompanyName
                href={
                  company_slug
                    ? `${paths.companies}/${company_slug}`
                    : `${paths.companies}`
                }
              >
                {company_name ? company_name : "Company name"}
              </CompanyName>
            </CompanyDetails>

            <CompanyAbout>
              <JobTextArea
                content={
                  about_job_company
                    ? `${about_job_company.split("</p>")[0]}</p>`
                    : `<p>The first line of your company description will appear here</p>`
                }
              />
            </CompanyAbout>
            {setApplyArea()}
            <Breadcrumbs category={category} />
            {props.router.pathname !== paths.post_job && (
              <AdWrapper>
                <JobListingsAdDesktop />
              </AdWrapper>
            )}
          </InnerApply>
        </JobApply>
      </JobWrapper>
    </>
  );
};

export default withRouter(NewJobPreviewDetails);
