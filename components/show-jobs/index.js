import Head from "next/head";
import JobListings from "../job-listings";
import Hero from "../new-hero";
import PopularCategories from "../popular-categories";
import RemoteResume from "../remote-resume";
import { useRef } from "react";
import { filterJobs } from "../../utils/filter";

export const ShowJobs = ({
  pageTitle,
  pageDesc,
  canonical,
  category,
  filters,
  filtersApplied,
  jobs,
}) => {
  const myRef = useRef(null);
  const jobsRef = useRef(null);

  const scrollToRef = () => {
    if (myRef) {
      window.scrollTo({
        top: myRef.current.offsetTop,
        behavior: "smooth",
      });
    }
  };

  const scrollToJobsRef = () => {
    if (jobsRef) {
      window.scrollTo({
        top: jobsRef.current.offsetTop - 80,
        behavior: "smooth",
      });
    }
  };
  const filteredJobs = jobs && filterJobs(jobs, filters);
  const featuredJobs = jobs && jobs.filter((job) => job.is_featured === true);

  return (
    <>
      <Head>
        <title>{pageTitle}</title>
        <meta name="description" content={pageDesc} />
        <link rel="canonical" href={canonical} />
      </Head>
      <div>
        <Hero
          handleScrollClick={scrollToRef}
          scrollToJobs={scrollToJobsRef}
          category={category}
        />
        <div ref={jobsRef} />
        <JobListings
          featuredJobs={featuredJobs}
          category={category}
          filters={filters}
          filtersApplied={filtersApplied}
          filteredJobs={filteredJobs}
          jobs={jobs}
        />
        <PopularCategories />
        <div ref={myRef} />
        <RemoteResume
          tracking={{
            action: `Resumeio - Job Listings ${category} - BottomHero`,
          }}
        />
      </div>
    </>
  );
};
