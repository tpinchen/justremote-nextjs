export const modalActions = {
  OPEN_MODAL: `OPEN_MODAL`,
  CLOSE_MODAL: `CLOSE_MODAL`
};

export const openModal = () => ({
  type: modalActions.OPEN_MODAL
});

export const closeModal = () => ({
  type: modalActions.CLOSE_MODAL
});
