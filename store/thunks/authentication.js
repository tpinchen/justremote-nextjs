import { Server } from "../../utils/server";
import { COOKIE_NAMES } from "../../constants/cookies";
import { setCookie, deleteCookie } from "../../utils/cookies";
import { userAuthorised, userNotAuthorised } from "../actions/auth-actions";
import { showMessage } from "../actions/messenger-actions";
import { paths } from "../../constants/paths";

export const SignUpUser = user => dispatch => {
  return Server({
    method: "post",
    url: "/api/v1/users",
    data: user
  })
    .then(function() {
      dispatch(SignInUser(user));
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const SignInUser = user => dispatch => {
  const { email, password } = user.user;
  return Server({
    method: "post",
    url: "/api/v1/user_token",
    data: { auth: { email: email, password: password } }
  })
    .then(function(response) {
      let token = response.data.jwt;
      setCookie(COOKIE_NAMES.TOKEN, token, 30);
      dispatch(userAuthorised());
      window.location.href = paths.user_profile;
    })
    .catch(function(error) {
      console.log(error);
      dispatch(showMessage("Incorrect email or password"));
    });
};

export const signOutUser = () => dispatch => {
  deleteCookie(COOKIE_NAMES.TOKEN);
  dispatch(userNotAuthorised());
  window.location.href = paths.auth;
};

export const Authorise = token => dispatch => {
  return Server({
    method: "get",
    url: "/api/v1/auth",
    headers: { Authorization: `Bearer ${token}` }
  })
    .then(function(response) {
      if (response.data && response.data.status === 200) {
        return dispatch(userAuthorised());
      } else {
        return dispatch(userNotAuthorised());
      }
    })
    .catch(function(error) {
      console.log(error);
    });
};
