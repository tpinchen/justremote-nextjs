import Link from "next/link";
import styled from "styled-components";
import { PageWrapper } from "../components/page-wrapper";
import { media } from "../constants/theme";
import { paths } from "../constants/paths";

const Inner = styled.div`
  padding: 25px;
  max-width: 900px;
  margin: 0 auto 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: calc(100vh - 60px - 139px);
  flex-direction: column;

  ${media.desktop`
    flex-direction:row;
  `}
`;

const Title = styled.h1`
  font-size: 22px;
  font-style: italic;
  margin-bottom: 5px;
`;

const SuccessImage = styled.img`
  border-radius: 3px;
  display: block;
  margin-bottom: 30px;
  max-width: 80%;

  ${media.desktop`
    width: 30%;
    margin-bottom:0;
  `}
`;
const LinkWrapper = styled.div`
  margin-bottom: 15px;

  a {
    text-decoration: underline;
  }
`;

const CopyWrap = styled.div`
  ${media.desktop`
    width: 70%;
    padding: 20px 40px;
  `}
`;

const SecondTitle = styled.h3`
  margin-bottom: 15px;
  margin-top: 30px;
`;

export const Subscribed = () => {
  return (
    <div>
      <PageWrapper>
        <Inner>
          <SuccessImage src="https://cdn.dribbble.com/users/400493/screenshots/1697984/bikerdribbble.gif" />
          <CopyWrap>
            <Title>You're about to receive awesome jobs!</Title>
            <p>
              Thanks so much for subscribing to our weekly jobs newsletter.
              We're just rushing around now to find you the best jobs but rest
              assured they'll be sent safely to your inbox once per week.
            </p>
            <p>Thanks again for supporting JustRemote.co</p>

            <SecondTitle>Where to next?</SecondTitle>
            <LinkWrapper>
              <Link passHref href={paths.home}>
                <a>Homepage</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.developer_jobs}>
                <a>Developer Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.design_jobs}>
                <a>Design Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.marketing_jobs}>
                <a>Marketing Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.manager_jobs}>
                <a>Manager/Exec Jobs</a>
              </Link>
            </LinkWrapper>
          </CopyWrap>
        </Inner>
      </PageWrapper>
    </div>
  );
};

export default Subscribed;
