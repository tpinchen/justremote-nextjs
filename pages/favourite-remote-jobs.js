import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";
import JobItem from "../components/job-item";
import { bindActionCreators } from "redux";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { PageWrapper } from "../components/page-wrapper";
import { Inner } from "../atoms/inner";
import { paths } from "../constants/paths";
import { fetchSingleFavouriteJob } from "../store/thunks/single-favourite-job";
import { setFavourite } from "../store/actions/favourite-actions";

const JobWrapper = styled.div`
  margin-bottom: 15px;
`;

const LinkWrapper = styled.div`
  margin-bottom: 15px;
`;

const UpperWrapper = styled.div`
  padding-bottom: 15px;
  margin-bottom: 15px;
  border-bottom: 1px solid #ddd;

  h2 {
    margin-bottom: 10px;
    font-style: italic;
    font-size: 16px;
  }
`;

const Favourites = (props) => {
  const { favourites } = props;
  return (
    <div>
      <Head>
        <title>Favourite Remote Positions - JustRemote</title>
        <meta
          name="description"
          content="View a full list of the remote jobs you've favourited."
        />
      </Head>
      <PageWrapper>
        {favourites.length === 0 ? (
          <Inner>
            <UpperWrapper>
              <h2>No favourites yet!</h2>
              <p>To favourite a job simply click the heart symbol.</p>
            </UpperWrapper>
            <LinkWrapper>
              <Link passHref href={paths.home}>
                <a>Homepage</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.developer_jobs}>
                <a>Developer Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.design_jobs}>
                <a>Design Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.marketing_jobs}>
                <a>Marketing Jobs</a>
              </Link>
            </LinkWrapper>
            <LinkWrapper>
              <Link passHref href={paths.manager_jobs}>
                <a>Manager/Exec Jobs</a>
              </Link>
            </LinkWrapper>
          </Inner>
        ) : null}

        {favourites.length > 0 && (
          <Inner>
            {props.favourites.map((job, i) => {
              return (
                <JobWrapper key={job.id}>
                  <JobItem
                    job={job}
                    selected={job.id === props.favouriteJob.id ? true : false}
                    handleFavouriteClick={props.handleFavouriteClick}
                    favourited={true}
                    history={props.history}
                    fetchSingleJob={props.fetchSingleFavouriteJob}
                  />
                </JobWrapper>
              );
            })}
          </Inner>
        )}
      </PageWrapper>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    favourites: state.favouriteState.favourites,
    favouriteJob: state.favouriteState.favouriteJob,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSingleFavouriteJob: bindActionCreators(
      fetchSingleFavouriteJob,
      dispatch
    ),
    handleFavouriteClick(job) {
      dispatch(setFavourite(job));
    },
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Favourites)
);
