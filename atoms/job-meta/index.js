import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const Wrapper = styled.div`
  margin-bottom: 20px;
  display: flex;
  justify-content: center;
  ${media.desktop`
    justify-content: left;
  `}
`;

const StyledJobMeta = styled.div`
  text-transform: capitalize;
  font-style: italic;
  font-size: 14px;
  letter-spacing: 0.02em;
  font-weight: 300;
  margin-bottom: 5px;
  margin-right: 32px;

  &:last-child {
    margin-right: 0;
  }
`;

export const JobMeta = (props) => {
  return (
    <Wrapper>
      <StyledJobMeta>{props.job_type}</StyledJobMeta>
      <StyledJobMeta>{props.remote_type}</StyledJobMeta>
    </Wrapper>
  );
};
