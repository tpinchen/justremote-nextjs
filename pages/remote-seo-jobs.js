import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getSEOJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return title.indexOf("seo") >= 0 || title.indexOf("search engine") >= 0;
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.marketing.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.marketing.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="seo"
      canonical={`https://justremote.co${paths.seo_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote SEO Jobs - JustRemote"
      pageDesc="Remote SEO Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getSEOJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
