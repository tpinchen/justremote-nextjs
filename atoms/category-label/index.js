import React from "react";
import styled from "styled-components";
import { media, colors } from "../../constants/theme";

const StyledCategoryLabel = styled.h1`
  font-size: ${(props) => (props.size ? props.size : "16px")};
  text-transform: uppercase;
  font-style: italic;
  letter-spacing: 1px;
  margin: ${(props) =>
    props.margin && props.margin.mobile ? props.margin.mobile : null};
  background-color: ${colors.primaryRed};
  display: inline-block;
  color: ${colors.white};
  padding: 5px 0px 5px 15px;
  border-radius: 0 3px 3px 0;
  position: relative;
  z-index: 0;
  font-weight: 400;

  &:before {
    content: " ";
    position: absolute;
    top: 0;
    right: -10px;
    width: 100%;
    height: 100%;
    background-color: ${colors.primaryRed};
    transform-origin: 100% 100%;
    transform: skewX(-20deg);
    z-index: -1;
  }

  ${media.desktop`
    margin: ${(props) =>
      props.margin && props.margin.desktop ? props.margin.desktop : null};
  `}
`;

export const CategoryTitle = (props) => {
  return (
    <StyledCategoryLabel
      margin={props.margin}
      color={props.color}
      size={props.size}
      bgColor={props.bgColor}
    >
      {props.title}
    </StyledCategoryLabel>
  );
};
