import React from "react";
import styled from "styled-components";
import { ErrorMessage } from "../error-message";

const StyledInput = styled.input`
  appearance: none;
  padding: 20px 15px;
  font-size: 16px;
  border: 1px solid ${props => (props.isInvalid ? "red" : "#ddd")};
  width: 100%;
  font-weight: 300;
  outline: none;
  border-radius: 3px;
`;

export const Input = props => {
  return (
    <div>
      <StyledInput
        isInvalid={props.validate ? props.validate.isInvalid : null}
        value={props.value}
        placeholder={props.placeholder}
        onChange={props.change}
        type={props.type ? props.type : null}
      />
      {props.validate ? (
        <ErrorMessage message={props.validate.message} />
      ) : null}
    </div>
  );
};
