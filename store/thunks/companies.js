import { Server } from "../../utils/server";
import { BASE_COMPANIES_API } from "../../constants/paths";
import {
  fetchingCompanies,
  fetchCompaniesSuccess,
  fetchCompaniesError
} from "../actions/companies-actions";

export const fetchCompanies = () => dispatch => {
  dispatch(fetchingCompanies());

  return Server({
    method: "get",
    url: BASE_COMPANIES_API
  })
    .then(response => response.data)
    .then(companies => {
      dispatch(fetchCompaniesSuccess(companies));
    })
    .catch(err => {
      dispatch(fetchCompaniesError(err));
    });
};
