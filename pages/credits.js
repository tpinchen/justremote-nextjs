import styled from "styled-components";
import Head from "next/head";
import { media } from "../constants/theme";
import { PageWrapper } from "../components/page-wrapper";
import { Inner } from "../atoms/inner";

const Wrapper = styled.div`
  max-width: 700px;
  margin: 0 auto;
`;

const PageHeading = styled.div`
  margin-bottom: 30px;
`;

const PageTitle = styled.h1`
  margin-bottom: 5px;
  font-size: 24px;
`;

const CreditBlock = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 60px;

  ${media.desktop`
    margin-bottom: 30px;
    flex-direction: row;
  `}
`;
const CreditDetails = styled.div`
  padding-left: 30px;
  padding-right: 30px;
`;
const CreditName = styled.h2`
  font-size: 18px;
  margin-bottom: 5px;
`;
const CreditLink = styled.a`
  text-decoration: underline;
`;
const Illustration = styled.img`
  width: 100%;
  border-radius: 4px;
  margin-bottom: 15px;

  ${media.desktop`
    width: 50%;
    margin-bottom: 0;
  `}
`;

export const Credits = () => {
  return (
    <div>
      <Head>
        <title>Illustration Credits - JustRemote</title>
        <meta
          name="description"
          content="Discover the makers of the amazing illustrations used throughout JustRemote's remote job site."
        />
      </Head>
      <PageWrapper>
        <Inner>
          <Wrapper>
            <PageHeading>
              <PageTitle>Illustration Credits</PageTitle>
              <p>
                Discover more from the amazing artists whose illustrations are
                used across our site.
              </p>
            </PageHeading>
            <CreditBlock>
              <Illustration src="/about-image-min.jpg" />
              <CreditDetails>
                <CreditName>Muhammed Sajid</CreditName>
                <CreditLink
                  href="https://www.behance.net/gallery/63498577/HOME"
                  target="_blank"
                >
                  Behance Portfolio
                </CreditLink>
              </CreditDetails>
            </CreditBlock>
            <CreditBlock>
              <Illustration src="/contact-image-min.jpg" />
              <CreditDetails>
                <CreditName>Muhammed Sajid</CreditName>
                <CreditLink
                  href="https://www.behance.net/gallery/63498577/HOME"
                  target="_blank"
                >
                  Behance Portfolio
                </CreditLink>
              </CreditDetails>
            </CreditBlock>
          </Wrapper>
        </Inner>
      </PageWrapper>
    </div>
  );
};

export default Credits;
