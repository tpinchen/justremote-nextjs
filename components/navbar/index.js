import React from "react";
import styled from "styled-components";
import { Logo } from "../../atoms/logo";
import { NavbarLinks } from "../navbar-links";
import { MenuDraw } from "../menu-draw";
import { paths } from "../../constants/paths";
import Link from "next/link";
import { useRouter } from "next/router";

const NavWrapper = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 24px 32px;
  width: 100%;
  z-index: 7;
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  ${(props) =>
    props.isNewJob &&
    `
    border-bottom: 1px solid #f4f6f7;
    position: fixed;
    background-color: #fff;
  `}
`;

export const Navbar = () => {
  const router = useRouter();
  const isNewJob = router.pathname.indexOf(paths.post_job) >= 0;
  return (
    <NavWrapper isNewJob={isNewJob}>
      <MenuDraw />
      <Link passHref href={paths.home}>
        <a>
          <Logo />
        </a>
      </Link>
      <NavbarLinks />
    </NavWrapper>
  );
};
