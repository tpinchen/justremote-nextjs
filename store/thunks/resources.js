import Prismic from "prismic-javascript";
import {
  fetchResourceSuccess,
  fetchingResource,
  fetchResourceError
} from "../actions/resource-actions";

export const fetchResource = uid => dispatch => {
  dispatch(fetchingResource());

  return Prismic.getApi("https://justremote.cdn.prismic.io/api/v2").then(
    api => {
      return api
        .getByUID("resources", uid)
        .then(response => {
          dispatch(fetchResourceSuccess(response));
        })
        .catch(error => {
          console.log(error);
          dispatch(fetchResourceError());
        });
    }
  );
};
