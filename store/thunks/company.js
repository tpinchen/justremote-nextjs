import { Server } from "../../utils/server";
import { BASE_COMPANIES_API } from "../../constants/paths";
import {
  fetchingCompany,
  fetchCompanySuccess,
  fetchCompanyError,
} from "../actions/company-actions";

export const fetchCompany = (companyId) => async (dispatch) => {
  dispatch(fetchingCompany());

  try {
    const response = await Server({
      method: "get",
      url: `${BASE_COMPANIES_API}/${companyId}`,
    });
    const company = response.data;
    dispatch(fetchCompanySuccess(company));
  } catch (err) {
    dispatch(fetchCompanyError(err));
  }
};
