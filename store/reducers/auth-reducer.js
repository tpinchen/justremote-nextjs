import { defaultAuthState } from "../state/auth-state";
import { authActions } from "../actions/auth-actions";

const handlers = {
  [authActions.USER_AUTHORISED]: (state, payload) => {
    return Object.assign({}, state, {
      loggedIn: true
    });
  },
  [authActions.USER_NOT_AUTHORISED]: (state, payload) => {
    return Object.assign({}, state, {
      loggedIn: false
    });
  }
};

const authReducer = (state = defaultAuthState, action) => {
  return handlers.hasOwnProperty(action.type)
    ? handlers[action.type](state, action.payload)
    : state;
};

export default authReducer;
