import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { rootReducer } from "./store/root-reducer";

// CREATING INITIAL STORE
export default (initialState) => {
  const isDev = process.env.NODE_ENV === "development";

  const store = createStore(
    rootReducer,
    initialState,
    isDev
      ? composeWithDevTools(applyMiddleware(thunkMiddleware))
      : applyMiddleware(thunkMiddleware)
  );

  // IF REDUCERS WERE CHANGED, RELOAD WITH INITIAL STATE
  if (module.hot) {
    module.hot.accept("./store/root-reducer", () => {
      const createNextReducer = require("./store/root-reducer").default;

      store.replaceReducer(createNextReducer(initialState));
    });
  }

  return store;
};
