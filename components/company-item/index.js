import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { paths } from "../../constants/paths";
import { media } from "../../constants/theme";

const StyledLink = styled.a`
  display: block;
  margin-bottom: 24px;
`;
const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

const CompanyLogo = styled.img`
  max-width: 30px;
  margin-right: 24px;
  ${media.desktop`
    max-width: 50px;
  `}
`;

const Left = styled.div`
  display: flex;
  align-items: center;
`;

const Country = styled.p`
  display: none;
  font-size: 14px;
  color: #aaa;
  letter-spacing: 1px;
  font-weight: 300;
  ${media.desktop`
    display:block;
  `}
`;

export const CompanyItem = ({ company }) => {
  return (
    <Link href={`${paths.companies}/${company.slug}`} passHref>
      <StyledLink>
        <Wrapper>
          <Left>
            {company.logo && company.logo.url && (
              <CompanyLogo
                src={company.logo.url}
                alt={`${company.name} logo`}
              />
            )}
            <p>{company.name}</p>
          </Left>
          <Country>{company.country}</Country>
        </Wrapper>
      </StyledLink>
    </Link>
  );
};
