import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";
import { PageWrapper } from "../../components/page-wrapper";
import { Inner } from "../../atoms/inner";
import { media } from "../../constants/theme";
import { paths, BASE_COMPANIES_API } from "../../constants/paths";
import { HomeCarbonAd } from "../../components/ads/home-carbon-ad";
import { Server } from "../../utils/server";

const Hero = styled.div`
  max-width: 720px;
  margin: 50px auto 50px;
  display: flex;
  flex-direction: column;
`;

const CompanyLogo = styled.img`
  max-width: 100px;
  border-radius: 3px;
  margin-bottom: 30px;
`;

const CompanyDescription = styled.div`
  font-size: 16px;
  color: rgba(0, 0, 0, 0.86);
  line-height: 1.6;
  font-weight: 400;
  word-wrap: break-word;
  text-rendering: optimizeLegibility;
  ${media.tablet`
    font-size: 18px;
  `}
`;

const CompanyName = styled.p`
  margin-bottom: 10px;
  font-size: 32px;
  font-weight: 700;
`;

const CompanyURL = styled.a`
  font-size: 14px;
  color: #aaa;
  letter-spacing: 1px;
`;

const JobsSection = styled.div`
  max-width: 720px;
  margin: 0 auto 50px;

  h1 {
    margin-bottom: 20px;
    font-size: 24px;
  }
`;

const LinkList = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
`;

const StyledLink = styled.a`
  margin-bottom: 10px;
  text-decoration: underline;
`;

const DiscoverMoreJobs = styled.div`
  margin-top: 60px;
`;

const Job = styled.div`
  a {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 15px;
    margin-bottom: 15px;
    border-radius: 3px;
    transition: 0.2s ease-in-out;
    border-left: 3px solid transparent;
    box-shadow: 0 0 4px 0px rgba(0, 0, 0, 0.15);
    min-height: 70px;

    &:hover {
      border-left: 3px solid #2bfeb9;
    }
  }

  p {
    margin: 0;
    padding: 0;
  }
`;

const AdWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 24px 0;
`;

const getCompanyId = (url) => {
  return url.split("/")[2]; // Return the second part of the URL
};

const createMarkup = (html) => {
  return { __html: html };
};

export const Company = (props) => {
  console.log(props);
  const { company } = props;
  return (
    <div>
      <Head>
        <title>Remote Jobs at {company ? company.name : ""} - JustRemote</title>
        <meta
          name="description"
          content={`Discover remote working jobs at ${
            company ? company.name : ""
          }. All job positions shown are live and can be applied for right now. Don't wait, apply today and start living the lifestyle of a remote worker.`}
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/medium-draft.css"
          // href="https://unpkg.com/medium-draft/dist/medium-draft.css"
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/basic.css"
          // href="https://unpkg.com/medium-draft/dist/basic.css"
        />
      </Head>
      <PageWrapper>
        <Inner>
          <Hero>
            {company.logo.url && (
              <CompanyLogo src={company.logo.url} alt={company.name} />
            )}
            <CompanyName>{company.name}</CompanyName>
            <CompanyURL
              href={`https://${company.url}`}
              rel="noopener noreferrer nofollow"
              target="_blank"
            >
              {company.url}
            </CompanyURL>
            <CompanyDescription
              dangerouslySetInnerHTML={createMarkup(company.description)}
            />
          </Hero>
          <JobsSection>
            <h1>{company.name} Jobs</h1>
            {company && company.jobs.length > 0 ? (
              company.jobs.map((job, i) => {
                return (
                  <Job key={i}>
                    <Link href={`/${job.href}`} passHref>
                      <a>
                        <h3>{job.title}</h3>
                        <p>{job.date}</p>
                      </a>
                    </Link>
                  </Job>
                );
              })
            ) : (
              <div>
                <p>No remote jobs are currently available at {company.name}</p>
                <DiscoverMoreJobs>
                  <h3>Discover Remote Jobs with other companies:</h3>
                  <LinkList>
                    <Link href={paths.developer_jobs} passHref>
                      <a>
                        <StyledLink>Remote Developer Jobs</StyledLink>
                      </a>
                    </Link>
                    <Link href={paths.marketing_jobs} passHref>
                      <a>
                        <StyledLink>Remote Marketing Jobs</StyledLink>
                      </a>
                    </Link>
                    <Link href={paths.design_jobs} passHref>
                      <a>
                        <StyledLink>Remote Design Jobs</StyledLink>
                      </a>
                    </Link>
                    <Link href={paths.manager_jobs} passHref>
                      <a>
                        <StyledLink>Remote Manager/Exec Jobs</StyledLink>
                      </a>
                    </Link>
                  </LinkList>
                </DiscoverMoreJobs>
              </div>
            )}
          </JobsSection>
          <AdWrapper>
            <HomeCarbonAd />
          </AdWrapper>
        </Inner>
      </PageWrapper>
    </div>
  );
};

export async function getStaticPaths() {
  const response = await Server({
    method: "get",
    url: BASE_COMPANIES_API,
  });

  const companies = response.data;

  const buildpaths = companies.map(
    (company) => `${paths.companies}/${company.slug}`
  );

  const result = {
    paths: buildpaths,
    fallback: false,
  };

  return result;
}

export async function getStaticProps({ params }) {
  const response = await Server({
    method: "get",
    url: `${BASE_COMPANIES_API}/${params[":id"]}`,
  });
  const company = response.data;

  return {
    props: {
      company,
    },
  };
}

export default Company;
