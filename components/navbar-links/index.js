import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { paths } from "../../constants/paths";
import { media, colors } from "../../constants/theme";
import { TrackClick } from "../../utils/analytics";
import { CountBadgeContainer } from "../count-badge";

const NavbarLinksWrapper = styled.div`
  ${media.desktop`
    display:flex;
    flex-direction: row;
    align-items:center;

    a {
      font-weight: 400;
      font-size: 16px;
      line-height: 18px;
    }
  `}
`;

const DesktopOnlyLinks = styled.div`
  display: none;
  ${media.desktop`
    display:block;
    a {
      margin-right: 24px;
    }
  `}
`;

const PostJob = styled.div`
  display: inline-block;

  a {
    border: 2px solid ${colors.primaryRed};
    padding: 10px 15px;
    border-radius: 4px;
    font-weight: 600;
    margin-right: 0;
  }
`;

const IconsWrapper = styled.div`
  margin-left: 24px;
`;

export const NavbarLinks = () => {
  return (
    <NavbarLinksWrapper>
      <DesktopOnlyLinks>
        <Link href={paths.companies} passHref>
          <a
            onClick={() =>
              TrackClick({
                category: "Navigation - Navbar",
                action: "Link Clicked",
                label: "Remote Companies",
              })
            }
          >
            Remote Companies
          </a>
        </Link>
        <PostJob>
          <Link passHref href={paths.post_job}>
            <a
              onClick={() =>
                TrackClick({
                  category: "Navigation - Navbar",
                  action: "Link Clicked",
                  label: "Post Job",
                })
              }
            >
              List your position
            </a>
          </Link>
        </PostJob>
      </DesktopOnlyLinks>

      <IconsWrapper>
        <Link passHref href={paths.favourite_jobs}>
          <a aria-label="Favourites" rel="nofollow">
            <CountBadgeContainer />
          </a>
        </Link>
      </IconsWrapper>
    </NavbarLinksWrapper>
  );
};
