import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getHRJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return (
      title.indexOf("people") >= 0 ||
      title.indexOf("hr") >= 0 ||
      title.indexOf("staff") >= 0 ||
      title.indexOf("resources") >= 0
    );
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.manager.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.manager.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="hr"
      canonical={`https://justremote.co${paths.hr_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote HR Jobs - JustRemote"
      pageDesc="Remote HR Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getHRJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
