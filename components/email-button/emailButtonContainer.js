import EmailButton from "./emailButton";
import { connect } from "react-redux";
import {
  showEmailForm,
  hideEmailForm,
} from "../../store/actions/email-form-actions";

const mapStateToProps = (state) => {
  return {
    open: state.emailFormState.open,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    showEmailForm() {
      dispatch(showEmailForm());
    },
    hideEmailForm() {
      dispatch(hideEmailForm());
    },
  };
};

export const EmailButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EmailButton);
