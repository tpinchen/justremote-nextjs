import Prismic from "prismic-javascript";
import {
  fetchPersonSuccess,
  fetchingPerson,
  fetchPersonError
} from "../actions/person-actions";

export const fetchPerson = uid => dispatch => {
  dispatch(fetchingPerson());

  return Prismic.getApi("https://justremote.cdn.prismic.io/api/v2").then(
    api => {
      return api
        .getByUID("person", uid)
        .then(response => {
          dispatch(fetchPersonSuccess(response));
        })
        .catch(error => {
          console.log(error);
          dispatch(fetchPersonError());
        });
    }
  );
};
