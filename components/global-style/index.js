import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  html, body, #root {
    padding: 0;
    margin:0;
    font-family: "europa", sans-serif;
  }

  input, textarea, select {
    font-family: "europa", sans-serif;
    letter-spacing: 0.03rem;
    font-weight: 400;
  }

  img {
    max-width: 100%;
    display:block;
  }

  * {
    box-sizing: border-box;
  }

  *:focus {
    outline:none;
  }
  h1,h2,h3,h4,h5 {
    margin: 0;
    padding: 0
  }
  ul {
    margin-left: 0px;
    padding: 0; 
  }
  a {
    text-decoration:none;
    color: #1b1b1b;
  }

  p {
    margin-bottom: 15px;
  }

  #carbonads {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial,
      sans-serif;
    position: relative;
  }
`;
