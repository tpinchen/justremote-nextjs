import React from "react";
import styled from "styled-components";
import { colors, media } from "../../constants/theme";
import { CompanyItem } from "../company-item";

const Wrapper = styled.div`
  background-color: ${colors.backgroundBlue};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 120px;
  ${media.desktop`
    padding-left: 32px;
    padding-right: 32px;
  `}
`;

const Panel = styled.div`
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.15);
  border-radius: 6px;
  background-color: ${colors.white};
  max-width: 800px;
  margin: 0 auto;
  width: 100%;
  ${media.desktop`
    width: 80%;
  `}
`;

const Right = styled.div`
  width: 100%;
  ${media.desktop`
    width: 100%;
`}
`;

const Flex = styled.div`
  width: 100%;
  padding: 16px 24px 16px;
  display: flex;
  flex-direction: column;
  ${media.desktop`
    flex-direction: row;
    padding: 60px;
  `}
`;

export const CompanyListings = ({ companies }) => (
  <Wrapper>
    <Panel>
      <Flex>
        <Right>
          {companies.map((company, i) => (
            <CompanyItem company={company} key={i} />
          ))}
        </Right>
      </Flex>
    </Panel>
  </Wrapper>
);

export default CompanyListings;
