import React from "react";
import styled from "styled-components";

const StyledJobWebsite = styled.div`
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.86);
  letter-spacing: 1px;
  font-weight: 400;
  font-size: 14px;
`;

export const JobWebsite = props => {
  return (
    <StyledJobWebsite>
      <a href={`http://${props.url}`} target="_blank" rel="noopener">
        {props.url}
      </a>
    </StyledJobWebsite>
  );
};
