export const authActions = {
  USER_AUTHORISED: `USER_AUTHORISED`,
  USER_NOT_AUTHORISED: `USER_NOT_AUTHORISED`
};

export const userAuthorised = () => ({
  type: authActions.USER_AUTHORISED
});

export const userNotAuthorised = () => ({
  type: authActions.USER_NOT_AUTHORISED
});