import { ShowJobs } from "../../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../../constants/paths";
import { Server } from "../../utils/server";

const getDesignJobs = (jobs) => {
  return jobs.filter((job) => job.category === "design");
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.design.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.design.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="design"
      canonical={`https://justremote.co${paths.design_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Design Jobs - JustRemote"
      pageDesc="Remote Design Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getDesignJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
