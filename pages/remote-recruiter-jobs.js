import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getRecruiterJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return (
      title.indexOf("recruiter") >= 0 ||
      title.indexOf("talent") >= 0 ||
      title.indexOf("recruiting") >= 0 ||
      title.indexOf("sourcer") >= 0
    );
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.manager.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.manager.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="recruiter"
      canonical={`https://justremote.co${paths.recruiter_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Recruiter Jobs - JustRemote"
      pageDesc="Remote Recruiter Jobs from around the globe. Remote jobs added daily so make sure to visit and favourite the best jobs for you."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getRecruiterJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
