import validator from "validator";
import FormValidator from "../utils/validator";

const EMPTY_TEXT_AREA = '<p class="md-block-unstyled"><br/></p>'; // This is the default clicked in but not typed of medium draft editor

export const newJobFormValidation = new FormValidator([
  {
    field: "about_job_company",
    method: validator.equals,
    args: [EMPTY_TEXT_AREA],
    validWhen: false,
    message: "Please provide some information about the company",
  },
  {
    field: "about_job_company",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide some information about the company",
  },
  {
    field: "title",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a job title",
  },
  {
    field: "job_location_valid",
    method: validator.isEmpty,
    // conditionalStateFields: [ // The validation will only run when these values in state are true else it will always validate successfully
    //   { remote_type : 'Partially Remote' },
    //   { overlap_required : true }
    // ],
    validWhen: false,
    message:
      "Please provide a location for this job. Ensure you select a location from the dropdown",
  },
  {
    field: "company_name",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a company name",
  },
  {
    field: "contact_first_name",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a first name",
  },
  {
    field: "contact_surname",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a surname",
  },
  {
    field: "contact_email",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a contact email",
  },
  {
    field: "contact_email",
    method: validator.isEmail,
    validWhen: true,
    message: "Please provide a valid contact email",
  },
  {
    field: "about_role",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide information about the role",
  },
  {
    field: "about_role",
    method: validator.equals,
    args: [EMPTY_TEXT_AREA],
    validWhen: false,
    message: "Please provide information about the role",
  },
  {
    field: "who_looking_for",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide some information about who you are looking for",
  },
  {
    field: "who_looking_for",
    method: validator.equals,
    args: [EMPTY_TEXT_AREA],
    validWhen: false,
    message: "Please provide some information about who you are looking for",
  },
  {
    field: "our_offer",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide some information about what you offer",
  },
  {
    field: "our_offer",
    method: validator.equals,
    args: [EMPTY_TEXT_AREA],
    validWhen: false,
    message: "Please provide some information about what you offer",
  },
  {
    field: "apply",
    method: validator.isEmpty,
    validWhen: false,
    message:
      "Please provide some information about how to apply for this position",
  },
  {
    field: "apply",
    method: validator.equals,
    args: [EMPTY_TEXT_AREA],
    validWhen: false,
    message:
      "Please provide some information about how to apply for this position",
  },
  {
    field: "job_type",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please select a job type",
  },
  {
    field: "job_category",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please select a job category",
  },
  {
    field: "company_url",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please provide a company website",
  },
  {
    field: "company_location_valid",
    method: validator.isEmpty,
    validWhen: false,
    message:
      "Please provide a headquarters location. Ensure to select a location from the dropdown.",
  },
  {
    field: "remote_type",
    method: validator.isEmpty,
    validWhen: false,
    message: "Please tell us whether the position is fully or partially remote",
  },
]);
