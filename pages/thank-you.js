import styled from "styled-components";
import { LinkButton } from "../atoms/button";
import { PageWrapper } from "../components/page-wrapper";
import { media } from "../constants/theme";
import { paths } from "../constants/paths";

const Inner = styled.div`
  padding: 25px;
  max-width: 900px;
  margin: 0 auto 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: calc(100vh - 60px - 139px);
  flex-direction: column;

  ${media.desktop`
    flex-direction:row;
  `}
`;

const Title = styled.h1`
  font-size: 22px;
  font-style: italic;
  margin-bottom: 5px;
`;

const SuccessImage = styled.img`
  border-radius: 3px;
  display: block;
  margin-bottom: 30px;
  max-width: 80%;

  ${media.desktop`
    width: 30%;
    margin-bottom:0;
  `}
`;

const CopyWrap = styled.div`
  ${media.desktop`
    width: 70%;
    padding: 20px 40px;
  `}
`;

export const ThankYou = () => {
  return (
    <div>
      <PageWrapper>
        <Inner>
          <SuccessImage src="https://cdn.dribbble.com/users/400493/screenshots/1697984/bikerdribbble.gif" />
          <CopyWrap>
            <Title>Job Posted!</Title>
            <p>
              Your job has been successfully submitted and will be live on our
              site very shortly!
            </p>
            <p>
              If for any reason you need to make changes please contact us via
              our contact page.
            </p>
            <p>Thanks again for supporting JustRemote.co</p>
            <LinkButton path={paths.post_job} title="List another job" small />
            <br />
            <LinkButton path={paths.home} title="Return to Homepage" small />
          </CopyWrap>
        </Inner>
      </PageWrapper>
    </div>
  );
};

export default ThankYou;
