import React, { Component } from 'react';
import styled from 'styled-components';
import ReactSelect from 'react-select';

const SelectWrapper = styled.div`
  display: block;
  border: 1px solid #ddd;
  padding: 10px;
  background-color: #fff;
`;

const StyledSelect = styled.select`
  appearance: none;
  font-size: 16px;
  border: none;
  width:100%;
  display:block;
  background-color: #fff;
`;

export class Select extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value : this.props.defaultState
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value}, () => {
      this.props.updateState && this.props.updateState(this.state.value);
    });
  }

  render() {
    const {
      options
    } = this.props;

    return(
      <SelectWrapper>
        <StyledSelect value={this.state.value} onChange={this.handleChange}>
          { options && options.map( (option, i) => {
            return <option key={`${option}-${i}`} value={option.value}>{option.title}</option>
          }) }
        </StyledSelect>
      </SelectWrapper>
    )
  }
}


const ReactSelectWrapper = styled.div`
  .Select--multi {
    .Select-placeholder {
      line-height: 52px;
      margin-left: 10px;
    }
    .Select-control {
      padding: 10px;
    }

    .Select-value {
      color: #fff;
      background-color: rgba(0,0,0,1);
    }
    .Select-value-icon {
      border-right: 1px solid rgba(255,255,255,0.25);

      &:hover {
        color: #ccc;
      }
    }

    .Select-menu-outer {      
      z-index:9;
      background-color: #fff;
    }
    
  }
`

export class SearchSelect extends Component {
  constructor(props) {
    super(props)

    this.state = {
      value : this.props.defaultValue
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    this.setState({ value }, () => {
      this.props.updateState && this.props.updateState(this.state.value);
    })
  }

  render() {
    const { 
      options,
      multi,
      closeOnSelect,
      removeSelected,
      clearable
    } = this.props

    return(
      <ReactSelectWrapper>
        <ReactSelect
          options={options} 
          multi={multi}
          clearable={clearable}
          closeOnSelect={closeOnSelect}
          removeSelected={removeSelected}
          value={this.state.value}
          onChange={this.handleChange}
        />
      </ReactSelectWrapper>
    )
  }
}