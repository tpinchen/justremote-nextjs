import React from "react";
import Head from "next/head";
import styled from "styled-components";
import NewJobPreviewDetails from "../../components/new-job-preview-details";
import { connect } from "react-redux";
import { OverlapHours } from "../../constants/overlap-hours";
import { RemoteDays } from "../../constants/remote-days";
import { selectableCountryList } from "../../constants/country-list";
import { toolList } from "../../constants/tool-list";
import { technologyList } from "../../constants/technology-list";
import { getCookie } from "../../utils/cookies";
import { PageWrapper } from "../../components/page-wrapper";
import { setNewJobDraft } from "../../store/actions/new-job-actions";
import { submitNewJob } from "../../store/thunks/new-job";
import { Prices } from "../../constants/prices";
// import { PreviewNewJob } from "../../components/preview-new-job";
import { StripePayment } from "../../components/stripe-payment";
import { PlacesTypeahead } from "../../components/places-typeahead";
import { Input } from "../../atoms/input";
import { FormField } from "../../atoms/form-field";
import { Label } from "../../atoms/label";
import { Select, SearchSelect } from "../../atoms/select";
import { Uploader } from "../../atoms/uploader";
import { EditTextArea } from "../../atoms/edit-text-area";
import { Inner } from "../../atoms/inner";
import { FlexContainer } from "../../atoms/flex-container";
import { PageDivider } from "../../atoms/page-divider";
import { CategoryTitle } from "../../atoms/category-label";
import { RadioChips } from "../../atoms/radio-chips";
import { Clients } from "../../atoms/clients";
import { colors } from "../../constants/theme";
import { withRouter } from "next/router";

const PageHero = styled.div`
  padding: 30px 0 0;
`;
const PageTitle = styled.h1`
  font-style: italic;
  font-size: 30px;
  margin-bottom: 10px;
`;
const NewJobForm = styled.div``;

const PreviewCopyWrapper = styled.div`
  border-bottom: 1px solid #f4f6f7;
  padding: 20px;
  margin-bottom: 30px;
`;

const StripeSVG = styled.svg`
  display: block;
  margin: 24px auto;
`;

const PreviewCopy = styled.p`
  font-size: 16px;
  font-style: italic;
  font-weight: 600;
  margin-top: 0;
  margin-bottom: 5px;
`;

const PreviewCopySmall = styled.p`
  font-size: 14px;
  color: #1b1b1b;
  margin: 0;
  font-weight: 300;
`;

const Small = styled.p`
  font-size: 14px;
  font-style: italic;
  margin-bottom: 0;
`;

const TooBusy = styled.div`
  a {
    color: ${colors.linkBlue};
  }
`;

export class NewJob extends React.Component {
  constructor(props) {
    super(props);

    if (this.props.draft) {
      this.state = this.props.draft;
    } else {
      this.state = {
        job_location_valid: "",
        job_city: "",
        job_county_state_short: "",
        job_county_state_long: "",
        job_country: "",
        job_formatted_location: "",
        job_latitude: "",
        job_longitude: "",
        company_location_valid: "",
        company_city: "",
        company_county_state_short: "",
        company_county_state_long: "",
        company_country: "",
        company_formatted_location: "",
        company_latitude: "",
        company_longitude: "",
        remote_days_count: 5,
        title: "",
        company_name: "",
        company_logo: [],
        company_type: "Inhouse",
        contact_first_name: "",
        contact_surname: "",
        contact_email: "",
        timezone: "GMT",
        overlap_required: false,
        overlap_hours: 0,
        specific_country_required: false,
        about_role: "",
        about_company: "",
        about_job_company: "",
        who_looking_for: "",
        our_offer: "",
        remote_type: "Fully Remote",
        apply: "",
        company_url: "",
        job_type: "",
        job_category: "",
        country_list: [],
        technology_list: [],
        tool_list: [],
        validation: null,
        rwa: null,
      };
    }
  }

  componentDidMount() {
    let rwa = getCookie("_rw_a");
    this.setState({ rwa: rwa });
  }

  validate(field) {
    const { validation } = this.state;

    return validation && validation[field].isInvalid ? validation[field] : null;
  }

  orderAlphabetically = (arr) => {
    const compare = function (a, b) {
      if (a.value < b.value) return -1;
      if (a.value > b.value) return 1;
      return 0;
    };

    return arr.sort(compare);
  };

  render() {
    let renderTimeOverlapFields;

    if (this.state.specific_country_required === false) {
      renderTimeOverlapFields = (
        <div>
          {this.state.remote_type === "Fully Remote" ? (
            <FormField>
              <Label
                title="Minimum Working Hour Overlap Requirements"
                tooltipTitle="Do you require that applicants work a minimum overlap with their colleagues.<br/><br/>This uses the Job Location to let applicants know there is a minimum overlap with that location.<br/><br/>You can currently only require that applicants have a minimum overlap <b>or</b> be from a specific country."
              />
              <RadioChips
                dark
                chips={[
                  {
                    label: "Yes",
                    selected:
                      this.state.overlap_required === true ? true : false,
                  },
                  {
                    label: "No",
                    selected:
                      this.state.overlap_required === false ? true : false,
                  },
                ]}
                click={(chip) => {
                  this.setState({
                    overlap_required: chip.label === "Yes" ? true : false,
                    overlap_hours: chip.label === "Yes" ? 1 : 0,
                  });
                }}
              />
            </FormField>
          ) : null}
          {this.state.overlap_required ? (
            <FormField>
              <Label
                title="How many hours of overlap"
                tooltipTitle="Select the number of overlap hours your company requires"
              />
              <Select
                defaultState={this.state.overlap_hours}
                options={OverlapHours}
                updateState={(value) => this.setState({ overlap_hours: value })}
              />
            </FormField>
          ) : null}
        </div>
      );
    }

    return (
      <div>
        <Head>
          <title>Post a Remote Job - JustRemote</title>
          <meta
            name="description"
            content={`Post a remote job to our amazing audience of remote workers. Single listings are just $${Prices.single_job} for a 30 day listing. Don't hesitate find your perfect remote worker today!`}
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/medium-draft.css"
            // href="https://unpkg.com/medium-draft/dist/medium-draft.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/medium-draft@0.5.18/dist/basic.css"
            // href="https://unpkg.com/medium-draft/dist/basic.css"
          />
          <link
            rel="stylesheet"
            href="https://unpkg.com/react-select@1.2.1/dist/react-select.css"
          />
          <link
            rel="stylesheet"
            href="https://unpkg.com/react-tippy@1.2.2/dist/tippy.css"
          />
        </Head>
        <PageWrapper>
          <FlexContainer>
            <PageDivider
              width={{
                desktop: "35%",
                mobile: "100%",
              }}
              position={{
                desktop: "fixed",
                mobile: "relative",
              }}
              height="calc(100vh - 78px)"
              overflow="scroll"
              display={{
                desktop: "block",
                mobile: "block",
              }}
            >
              {/* <PreviewNewJob job={this.state} /> */}

              <Inner>
                <PageHero>
                  <PageTitle>Post a job</PageTitle>
                  <p>
                    Reach our audience of global remote workers for just{" "}
                    <b>${Prices.single_job} for a 30 day listing</b>.
                  </p>

                  <Small>
                    As a minimum positions must allow 2 days remote working per
                    week to list on JustRemote.
                  </Small>
                </PageHero>
              </Inner>
              <Clients />
              <Inner>
                <TooBusy>
                  <p>
                    Too busy to complete the form? Send an email to{" "}
                    <a href="mailto:contact@justremote.co" target="_blank">
                      contact@justremote.co
                    </a>{" "}
                    with a link or attachment of your listing and we'll post it
                    for you!
                  </p>
                </TooBusy>
              </Inner>
              <NewJobForm>
                <CategoryTitle
                  margin={{
                    desktop: "20px 0 0 0",
                    mobile: "20px 0 0 0",
                  }}
                  title="1. Listing Type"
                  size="11px"
                />
                <Inner>
                  <FormField>
                    <Label title="Are you inhouse or a recruiter?*" />
                    <RadioChips
                      dark
                      chips={[
                        {
                          label: "Inhouse",
                          selected:
                            this.state.company_type === "Inhouse"
                              ? true
                              : false,
                        },
                        {
                          label: "Recruiter",
                          selected:
                            this.state.company_type === "Recruiter"
                              ? true
                              : false,
                        },
                      ]}
                      click={(chip) => {
                        this.setState({ company_type: chip.label });
                      }}
                    />
                  </FormField>
                </Inner>
                <CategoryTitle
                  margin={{
                    desktop: "0px 0 0 0",
                    mobile: "0px 0 0 0",
                  }}
                  title="2. Tell us about the role"
                  size="11px"
                />
                <Inner>
                  <FormField>
                    <Label title="Job Title*" />
                    <Input
                      validate={this.validate("title")}
                      value={this.state.title}
                      change={(e) => {
                        this.setState({ title: e.target.value });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label title="Job Type*" />
                    <RadioChips
                      dark
                      validate={this.validate("job_type")}
                      chips={[
                        {
                          label: "permanent",
                          selected:
                            this.state.job_type === "permanent" ? true : false,
                        },
                        {
                          label: "contract",
                          selected:
                            this.state.job_type === "contract" ? true : false,
                        },
                      ]}
                      click={(chip) => {
                        this.setState({ job_type: chip.label });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Fully or partially Remote*"
                      tooltipTitle={
                        "We require that all roles listed are remote at least 2 days per week. Is the role fully remote (5 days per week) or partially remote (2-4 days per week)?"
                      }
                    />
                    <RadioChips
                      dark
                      validate={this.validate("remote_type")}
                      chips={[
                        {
                          label: "Fully Remote",
                          selected:
                            this.state.remote_type === "Fully Remote"
                              ? true
                              : false,
                        },
                        {
                          label: "Partially Remote",
                          selected:
                            this.state.remote_type === "Partially Remote"
                              ? true
                              : false,
                        },
                      ]}
                      click={(chip) => {
                        const isPartiallyRemote =
                          chip.label === "Partially Remote";

                        this.setState({
                          remote_type: chip.label,
                          overlap_required: isPartiallyRemote
                            ? false
                            : this.state.overlap_required,
                          overlap_hours: isPartiallyRemote
                            ? 0
                            : this.state.overlap_hours,
                          specific_country_required: isPartiallyRemote
                            ? false
                            : this.state.specific_country_required,
                          country_list: isPartiallyRemote
                            ? []
                            : this.state.country_list,
                          remote_days_count: isPartiallyRemote ? 2 : 5,
                        });
                      }}
                    />
                  </FormField>
                  {this.state.remote_type === "Partially Remote" ? (
                    <FormField>
                      <Label
                        title="How many days will be remote?"
                        tooltipTitle="Select how many days of remote working per week the applicant will be entitled to."
                      />
                      <Select
                        defaultState={this.state.remote_days_count}
                        options={RemoteDays}
                        updateState={(value) =>
                          this.setState({ remote_days_count: value })
                        }
                      />
                    </FormField>
                  ) : null}
                  <FormField>
                    <Label
                      tooltipTitle="Below are our core categories. Subcategories are determined by words used in the Job Title"
                      title="Job Category*"
                    />
                    <RadioChips
                      dark
                      validate={this.validate("job_category")}
                      chips={[
                        {
                          label: "developer",
                          selected:
                            this.state.job_category === "developer"
                              ? true
                              : false,
                        },
                        {
                          label: "marketing",
                          selected:
                            this.state.job_category === "marketing"
                              ? true
                              : false,
                        },
                        {
                          label: "design",
                          selected:
                            this.state.job_category === "design" ? true : false,
                        },
                        {
                          label: "manager/exec",
                          selected:
                            this.state.job_category === "manager"
                              ? true
                              : false,
                        },
                        {
                          label: "customer service",
                          selected:
                            this.state.job_category === "customerservice"
                              ? true
                              : false,
                        },
                        {
                          label: "writing",
                          selected:
                            this.state.job_category === "writing"
                              ? true
                              : false,
                        },
                      ]}
                      click={(chip) => {
                        let category = chip.label;
                        if (chip.label === "manager/exec") {
                          category = "manager";
                        }
                        if (chip.label === "customer service") {
                          category = "customerservice";
                        }
                        this.setState({ job_category: category });
                      }}
                    />
                  </FormField>
                  {this.state.job_category === "developer" ? (
                    <FormField>
                      <Label title="Technology Stack*" />
                      <SearchSelect
                        defaultValue={this.state.technology_list}
                        options={this.orderAlphabetically(technologyList)}
                        multi={true}
                        closeOnSelect={false}
                        removeSelected={true}
                        clearable={false}
                        updateState={(value) =>
                          this.setState({ technology_list: value })
                        }
                      />
                    </FormField>
                  ) : null}
                  {this.state.company_type === "Recruiter" ? (
                    <FormField>
                      <Label
                        title="About the Company*"
                        tooltipTitle="An overview of the company the applicant will be working for."
                      />
                      <EditTextArea
                        validate={this.validate("about_job_company")}
                        value={this.state.about_job_company}
                        change={(html) => {
                          this.setState({
                            about_job_company: html,
                            about_company: html,
                          });
                        }}
                      />
                    </FormField>
                  ) : null}
                  <FormField>
                    <Label
                      title="About the Role*"
                      tooltipTitle="What will the applicant be doing day to day? Which team will they be joining?"
                    />
                    <EditTextArea
                      validate={this.validate("about_role")}
                      value={this.state.about_role}
                      change={(html) => {
                        this.setState({ about_role: html });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Experience*"
                      tooltipTitle="What experience and skills are you looking for. Years experience etc."
                    />
                    <EditTextArea
                      validate={this.validate("who_looking_for")}
                      value={this.state.who_looking_for}
                      change={(html) => {
                        this.setState({ who_looking_for: html });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Salary & Perks*"
                      tooltipTitle="What perks do you offer? Competitive salary, paid holiday etc."
                    />
                    <EditTextArea
                      validate={this.validate("our_offer")}
                      value={this.state.our_offer}
                      change={(html) => {
                        this.setState({ our_offer: html });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="How should people apply*"
                      tooltipTitle="Should the applicant email someone specific? Visit a specific URL or your website etc.<br/><br/>To add a Quick Apply button just highlight a url below and turn it into a link using the highlight tools. The Quick Apply button will then use this url as its destination."
                    />
                    <EditTextArea
                      validate={this.validate("apply")}
                      value={this.state.apply}
                      hyperlink={true}
                      change={(html) => {
                        this.setState({ apply: html });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Team Tools"
                      tooltipTitle="What kind of tools do your teams use to keep in touch and manage work."
                    />
                    <SearchSelect
                      defaultValue={this.state.tool_list}
                      options={this.orderAlphabetically(toolList)}
                      multi={true}
                      closeOnSelect={false}
                      removeSelected={true}
                      clearable={false}
                      updateState={(value) =>
                        this.setState({ tool_list: value })
                      }
                    />
                  </FormField>
                  {this.state.remote_type === "Fully Remote" ? (
                    <FormField>
                      <Label
                        title="Resident of specific country requirements"
                        tooltipTitle="Let applicants know you're only accepting applications from specific countries."
                      />
                      <RadioChips
                        dark
                        chips={[
                          {
                            label: "Yes",
                            selected:
                              this.state.specific_country_required === true
                                ? true
                                : false,
                          },
                          {
                            label: "No",
                            selected:
                              this.state.specific_country_required === false
                                ? true
                                : false,
                          },
                        ]}
                        click={(chip) => {
                          this.setState({
                            specific_country_required:
                              chip.label === "Yes" ? true : false,
                            overlap_required: false,
                            overlap_hours: 0,
                          });
                        }}
                      />
                    </FormField>
                  ) : null}
                  {this.state.remote_type === "Fully Remote" &&
                  this.state.specific_country_required ? (
                    <FormField>
                      <Label
                        title="Which country/countries"
                        tooltipTitle="Select countries that you are accepting applications from."
                      />
                      <SearchSelect
                        defaultValue={this.state.country_list}
                        options={selectableCountryList()}
                        multi={true}
                        closeOnSelect={true}
                        removeSelected={true}
                        clearable={false}
                        updateState={(value) =>
                          this.setState({ country_list: value })
                        }
                      />
                    </FormField>
                  ) : null}
                  {renderTimeOverlapFields}

                  <FormField>
                    <Label
                      title="Job Location*"
                      tooltipTitle="The location associated with this position.<br/><br/>For example if the applicant will be working with others who are based in an office in London then the location would be 'London, UK'.<br/><br/><i>**<b><u>Not</u></b> where the applicant is based**</i>"
                    />
                    <PlacesTypeahead
                      validate={this.validate("job_location_valid")}
                      validateLocation={(value) =>
                        this.setState({ job_location_valid: value })
                      }
                      setLocation={(obj) =>
                        this.setState({
                          job_city: obj.city,
                          job_county_state_short: obj.county_state_short,
                          job_county_state_long: obj.county_state_long,
                          job_country: obj.country,
                          job_formatted_location: obj.formatted_location,
                          job_latitude: obj.latitude,
                          job_longitude: obj.longitude,
                        })
                      }
                    />
                  </FormField>
                </Inner>

                <CategoryTitle
                  margin={{
                    desktop: "0 0 0 0",
                    mobile: "0 0 0 0",
                  }}
                  title={`3. Tell us about your ${
                    this.state.company_type === "Recruiter" ? "recruitment" : ""
                  } company`}
                  size="11px"
                />
                <Inner>
                  <FormField>
                    <Label title="Company Name*" />
                    <Input
                      value={this.state.company_name}
                      validate={this.validate("company_name")}
                      change={(e) => {
                        this.setState({ company_name: e.target.value });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Company Headquarters*"
                      tooltipTitle="Your company location. <br/><br/>If your company is global we recommend putting the location where the company was founded.<br/><br/>For example if it was founded in New York and is a registered company in the United States then enter 'New York'"
                    />
                    <PlacesTypeahead
                      validate={this.validate("company_location_valid")}
                      validateLocation={(value) =>
                        this.setState({ company_location_valid: value })
                      }
                      setLocation={(obj) =>
                        this.setState({
                          company_city: obj.city,
                          company_county_state_short: obj.county_state_short,
                          company_county_state_long: obj.county_state_long,
                          company_country: obj.country,
                          company_formatted_location: obj.formatted_location,
                          company_latitude: obj.latitude,
                          company_longitude: obj.longitude,
                        })
                      }
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Company Website*"
                      tooltipTitle={
                        "Your company's website address. Please <b>don't</b> include 'http://' or 'https://'"
                      }
                    />
                    <Input
                      value={this.state.company_url}
                      validate={this.validate("company_url")}
                      change={(e) => {
                        this.setState({ company_url: e.target.value });
                      }}
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title="Company Logo*"
                      tooltipTitle="Add your company logo. Ideal measurements are 160x160px and files no larger than 150kb"
                    />
                    <Uploader
                      updateState={(file) =>
                        this.setState({ company_logo: file }, () =>
                          console.log(this.state)
                        )
                      }
                      mazSize={153600} // 150 kb
                      copy={
                        "Drag in a file or click here to upload a logo (jpeg/png only)"
                      }
                      accept="image/jpeg, image/png"
                    />
                  </FormField>
                  {this.state.company_type === "Inhouse" ? (
                    <FormField>
                      <Label
                        title="About your company*"
                        tooltipTitle="Tell applicants about your company.<br/><br/>The first sentence will appear on your job listing, the rest will appear on your company profile page."
                      />
                      <EditTextArea
                        validate={this.validate("about_job_company")}
                        value={this.state.about_company}
                        change={(html) => {
                          this.setState({
                            about_company: html,
                            about_job_company: html,
                          });
                        }}
                      />
                    </FormField>
                  ) : null}
                </Inner>
                <CategoryTitle
                  margin={{
                    desktop: "30px 0 0 0",
                    mobile: "30px 0 0 0",
                  }}
                  title="4. Payment details"
                  size="11px"
                />
                <Inner>
                  <FormField>
                    <Label title={"Your First Name*"} />
                    <Input
                      validate={this.validate("contact_first_name")}
                      value={this.state.contact_first_name}
                      change={(e) =>
                        this.setState({ contact_first_name: e.target.value })
                      }
                    />
                  </FormField>
                  <FormField>
                    <Label title={"Your Surname*"} />
                    <Input
                      validate={this.validate("contact_surname")}
                      value={this.state.contact_surname}
                      change={(e) =>
                        this.setState({ contact_surname: e.target.value })
                      }
                    />
                  </FormField>
                  <FormField>
                    <Label
                      title={"Your Email*"}
                      tooltipTitle="This email address will receive all updates about your job listing"
                    />
                    <Input
                      validate={this.validate("contact_email")}
                      value={this.state.contact_email}
                      change={(e) =>
                        this.setState({ contact_email: e.target.value })
                      }
                    />
                  </FormField>
                  <StripePayment
                    formState={this.state}
                    pushLocation={this.props.router.push}
                    submitNewJob={this.props.submitNewJob}
                    setValidation={(invalidState) => {
                      this.setState({
                        validation: invalidState,
                      });
                    }}
                  />
                  <StripeSVG
                    xmlns="http://www.w3.org/2000/svg"
                    width="119px"
                    height="26px"
                  >
                    <path
                      fillRule="evenodd"
                      opacity="0.349"
                      fill="rgb(66, 71, 112)"
                      d="M113.000,26.000 L6.000,26.000 C2.686,26.000 -0.000,23.314 -0.000,20.000 L-0.000,6.000 C-0.000,2.686 2.686,-0.000 6.000,-0.000 L113.000,-0.000 C116.314,-0.000 119.000,2.686 119.000,6.000 L119.000,20.000 C119.000,23.314 116.314,26.000 113.000,26.000 ZM118.000,6.000 C118.000,3.239 115.761,1.000 113.000,1.000 L6.000,1.000 C3.239,1.000 1.000,3.239 1.000,6.000 L1.000,20.000 C1.000,22.761 3.239,25.000 6.000,25.000 L113.000,25.000 C115.761,25.000 118.000,22.761 118.000,20.000 L118.000,6.000 Z"
                    />
                    <path
                      fillRule="evenodd"
                      opacity="0.502"
                      fill="rgb(66, 71, 112)"
                      d="M60.700,18.437 L59.395,18.437 L60.405,15.943 L58.395,10.871 L59.774,10.871 L61.037,14.323 L62.310,10.871 L63.689,10.871 L60.700,18.437 ZM55.690,16.259 C55.238,16.259 54.774,16.091 54.354,15.764 L54.354,16.133 L53.007,16.133 L53.007,8.566 L54.354,8.566 L54.354,11.229 C54.774,10.913 55.238,10.745 55.690,10.745 C57.100,10.745 58.068,11.881 58.068,13.502 C58.068,15.122 57.100,16.259 55.690,16.259 ZM55.406,11.902 C55.038,11.902 54.669,12.060 54.354,12.376 L54.354,14.628 C54.669,14.943 55.038,15.101 55.406,15.101 C56.164,15.101 56.690,14.449 56.690,13.502 C56.690,12.555 56.164,11.902 55.406,11.902 ZM47.554,15.764 C47.144,16.091 46.681,16.259 46.218,16.259 C44.818,16.259 43.840,15.122 43.840,13.502 C43.840,11.881 44.818,10.745 46.218,10.745 C46.681,10.745 47.144,10.913 47.554,11.229 L47.554,8.566 L48.912,8.566 L48.912,16.133 L47.554,16.133 L47.554,15.764 ZM47.554,12.376 C47.249,12.060 46.881,11.902 46.513,11.902 C45.744,11.902 45.218,12.555 45.218,13.502 C45.218,14.449 45.744,15.101 46.513,15.101 C46.881,15.101 47.249,14.943 47.554,14.628 L47.554,12.376 ZM39.535,13.870 C39.619,14.670 40.251,15.217 41.134,15.217 C41.619,15.217 42.155,15.038 42.702,14.722 L42.702,15.849 C42.103,16.122 41.503,16.259 40.913,16.259 C39.324,16.259 38.209,15.101 38.209,13.460 C38.209,11.871 39.303,10.745 40.808,10.745 C42.187,10.745 43.123,11.829 43.123,13.375 C43.123,13.523 43.123,13.691 43.102,13.870 L39.535,13.870 ZM40.756,11.786 C40.103,11.786 39.598,12.271 39.535,12.997 L41.829,12.997 C41.787,12.281 41.356,11.786 40.756,11.786 ZM35.988,12.618 L35.988,16.133 L34.641,16.133 L34.641,10.871 L35.988,10.871 L35.988,11.397 C36.367,10.976 36.830,10.745 37.282,10.745 C37.430,10.745 37.577,10.755 37.724,10.797 L37.724,11.997 C37.577,11.955 37.409,11.934 37.251,11.934 C36.809,11.934 36.335,12.176 35.988,12.618 ZM29.979,13.870 C30.063,14.670 30.694,15.217 31.578,15.217 C32.062,15.217 32.599,15.038 33.146,14.722 L33.146,15.849 C32.546,16.122 31.946,16.259 31.357,16.259 C29.768,16.259 28.653,15.101 28.653,13.460 C28.653,11.871 29.747,10.745 31.252,10.745 C32.630,10.745 33.567,11.829 33.567,13.375 C33.567,13.523 33.567,13.691 33.546,13.870 L29.979,13.870 ZM31.199,11.786 C30.547,11.786 30.042,12.271 29.979,12.997 L32.273,12.997 C32.231,12.281 31.799,11.786 31.199,11.786 ZM25.274,16.133 L24.200,12.555 L23.137,16.133 L21.927,16.133 L20.117,10.871 L21.464,10.871 L22.527,14.449 L23.590,10.871 L24.810,10.871 L25.873,14.449 L26.936,10.871 L28.283,10.871 L26.484,16.133 L25.274,16.133 ZM17.043,16.259 C15.454,16.259 14.328,15.112 14.328,13.502 C14.328,11.881 15.454,10.745 17.043,10.745 C18.632,10.745 19.748,11.881 19.748,13.502 C19.748,15.112 18.632,16.259 17.043,16.259 ZM17.043,11.871 C16.254,11.871 15.707,12.534 15.707,13.502 C15.707,14.470 16.254,15.133 17.043,15.133 C17.822,15.133 18.369,14.470 18.369,13.502 C18.369,12.534 17.822,11.871 17.043,11.871 ZM11.128,13.533 L9.918,13.533 L9.918,16.133 L8.571,16.133 L8.571,8.892 L11.128,8.892 C12.602,8.892 13.654,9.850 13.654,11.218 C13.654,12.586 12.602,13.533 11.128,13.533 ZM10.939,9.987 L9.918,9.987 L9.918,12.439 L10.939,12.439 C11.718,12.439 12.265,11.944 12.265,11.218 C12.265,10.482 11.718,9.987 10.939,9.987 Z"
                    />
                    <path
                      fillRule="evenodd"
                      opacity="0.502"
                      fill="rgb(66, 71, 112)"
                      d="M111.116,14.051 L105.557,14.051 C105.684,15.382 106.659,15.774 107.766,15.774 C108.893,15.774 109.781,15.536 110.555,15.146 L110.555,17.433 C109.784,17.861 108.765,18.169 107.408,18.169 C104.642,18.169 102.704,16.437 102.704,13.013 C102.704,10.121 104.348,7.825 107.049,7.825 C109.746,7.825 111.154,10.120 111.154,13.028 C111.154,13.303 111.129,13.898 111.116,14.051 ZM107.031,10.140 C106.321,10.140 105.532,10.676 105.532,11.955 L108.468,11.955 C108.468,10.677 107.728,10.140 107.031,10.140 ZM98.108,18.169 C97.114,18.169 96.507,17.750 96.099,17.451 L96.093,20.664 L93.254,21.268 L93.253,8.014 L95.753,8.014 L95.901,8.715 C96.293,8.349 97.012,7.825 98.125,7.825 C100.119,7.825 101.997,9.621 101.997,12.927 C101.997,16.535 100.139,18.169 98.108,18.169 ZM97.446,10.340 C96.795,10.340 96.386,10.578 96.090,10.903 L96.107,15.122 C96.383,15.421 96.780,15.661 97.446,15.661 C98.496,15.661 99.200,14.518 99.200,12.989 C99.200,11.504 98.485,10.340 97.446,10.340 ZM89.149,8.014 L91.999,8.014 L91.999,17.966 L89.149,17.966 L89.149,8.014 ZM89.149,4.836 L91.999,4.230 L91.999,6.543 L89.149,7.149 L89.149,4.836 ZM86.110,11.219 L86.110,17.966 L83.272,17.966 L83.272,8.014 L85.727,8.014 L85.905,8.853 C86.570,7.631 87.897,7.879 88.275,8.015 L88.275,10.625 C87.914,10.508 86.781,10.338 86.110,11.219 ZM80.024,14.475 C80.024,16.148 81.816,15.627 82.179,15.482 L82.179,17.793 C81.801,18.001 81.115,18.169 80.187,18.169 C78.502,18.169 77.237,16.928 77.237,15.247 L77.250,6.138 L80.022,5.548 L80.024,8.014 L82.180,8.014 L82.180,10.435 L80.024,10.435 L80.024,14.475 ZM76.485,14.959 C76.485,17.003 74.858,18.169 72.497,18.169 C71.518,18.169 70.448,17.979 69.392,17.525 L69.392,14.814 C70.345,15.332 71.559,15.721 72.500,15.721 C73.133,15.721 73.589,15.551 73.589,15.026 C73.589,13.671 69.273,14.181 69.273,11.038 C69.273,9.028 70.808,7.825 73.111,7.825 C74.052,7.825 74.992,7.969 75.933,8.344 L75.933,11.019 C75.069,10.552 73.972,10.288 73.109,10.288 C72.514,10.288 72.144,10.460 72.144,10.903 C72.144,12.181 76.485,11.573 76.485,14.959 Z"
                    />
                  </StripeSVG>

                  <TooBusy style={{ textAlign: "center" }}>
                    <p>
                      Having trouble listing? Send an email to{" "}
                      <a href="mailto:contact@justremote.co" target="_blank">
                        contact@justremote.co
                      </a>{" "}
                      with a link or attachment of your listing and we'll post
                      it for you!
                    </p>
                  </TooBusy>
                </Inner>
              </NewJobForm>
            </PageDivider>
            <PageDivider
              width={{
                desktop: "65%",
                mobile: "0%",
              }}
              margin="0px 0 30px 35%"
              display={{
                desktop: "block",
                mobile: "none",
              }}
            >
              <PreviewCopyWrapper>
                <PreviewCopy>Preview:</PreviewCopy>
                <PreviewCopySmall>
                  Complete the form on the left to watch your listing come to
                  life.
                </PreviewCopySmall>
              </PreviewCopyWrapper>

              {this.state ? (
                <NewJobPreviewDetails schema={false} job={this.state} />
              ) : null}
            </PageDivider>
          </FlexContainer>
        </PageWrapper>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { draft: state.newJobState.draft };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveNewJobDraft(draft) {
      dispatch(setNewJobDraft(draft));
    },
    submitNewJob(state, push, token, provider) {
      dispatch(submitNewJob(state, push, token, provider));
    },
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewJob));
