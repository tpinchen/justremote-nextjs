import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getProjectManagerJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return (
      title.indexOf("product manager") >= 0 || title.indexOf("project") >= 0
    );
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.manager.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.manager.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="project manager"
      canonical={`https://justremote.co${paths.project_manager_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Project Manager Jobs - JustRemote"
      pageDesc="Remote Project Manager Jobs from around the globe. Freelance and full time writing jobs for project managers, product managers and more."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getProjectManagerJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
