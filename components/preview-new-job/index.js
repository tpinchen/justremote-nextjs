import React, { Component } from "react";
import styled from "styled-components";
import Drawer from "@material-ui/core/Drawer";
import NewJobPreviewDetails from "../new-job-preview-details";
import { withStyles } from "@material-ui/core/styles";
import { PreviewJobButton } from "../../atoms/preview-job-button";

const styles = {
  paper: {
    minWidth: "100%",
  },
};

const DrawInner = styled.div`
  padding: 20px;
`;
const Close = styled.div`
  float: right;
  font-size: 30px;
  &:hover {
    cursor: pointer;
  }
`;
const PreviewCopy = styled.p`
  font-size: 16px;
  font-style: italic;
  margin-top: 9px;
  border-bottom: 1px solid #ddd;
  padding-bottom: 20px;
  font-weight: 600;
`;

class WrappedPreviewNewJob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }
  render() {
    const classes = this.props.classes;
    return (
      <div>
        <Drawer
          anchor="right"
          onClose={() => this.setState({ open: false })}
          open={this.state.open}
          classes={{
            paper: classes.paper,
          }}
        >
          <DrawInner>
            <Close onClick={() => this.setState({ open: false })}>
              <i className="far fa-times-circle" />
            </Close>
            <PreviewCopy>Preview of your job</PreviewCopy>
          </DrawInner>
          <NewJobPreviewDetails job={this.props.job} />
        </Drawer>
        <PreviewJobButton onClick={() => this.setState({ open: true })} />
      </div>
    );
  }
}

export const PreviewNewJob = withStyles(styles)(WrappedPreviewNewJob);
