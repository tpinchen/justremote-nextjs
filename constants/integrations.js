export const setStripePublicKey = () => {
  if (process.env.NODE_ENV === "development") {
    return "pk_test_8f44ncIL1RYYQoS7eU9rxdqk";
  } else {
    return "pk_live_FHsvknkUGMLrrV1syLv3fgrz";
  }
};
