import Prismic from "prismic-javascript";
import {
  fetchArticleSuccess,
  fetchingArticle,
  fetchArticleError
} from "../actions/article-actions";

export const fetchArticle = uid => dispatch => {
  dispatch(fetchingArticle());

  return Prismic.getApi("https://justremote.cdn.prismic.io/api/v2").then(
    api => {
      return api
        .getByUID("post", uid, {
          fetchLinks: [
            "author.name",
            "author.avatar",
            "author.position_and_website",
            "author.author_website",
            "author.twitter_handle",
            "author.medium_handle"
          ]
        })
        .then(response => {
          dispatch(fetchArticleSuccess(response));
        })
        .catch(error => {
          console.log(error);
          dispatch(fetchArticleError());
        });
    }
  );
};
