import React from "react";
import { withRouter } from "next/router";
import { paths } from "../../constants/paths";
import { LinkChip } from "../../atoms/chip";
import { Filter } from "../../atoms/filter";
import { FilterLabel } from "../../atoms/filter-label";
import {
  isMarketingRoute,
  isManagerRoute,
  isWritingRoute,
} from "../../utils/route-helpers";

export const AllJobFilters = ({ location }) => (
  <Filter>
    <FilterLabel>Job Category</FilterLabel>
    <LinkChip
      selected={location.pathname === paths.all_jobs}
      label="All"
      path={paths.all_jobs}
    />
    <LinkChip
      selected={location.pathname === paths.customer_service_jobs}
      label="Customer Service"
      path={paths.customer_service_jobs}
    />
    <LinkChip
      selected={location.pathname === paths.developer_jobs}
      label="Developer"
      path={paths.developer_jobs}
    />
    <LinkChip
      selected={location.pathname === paths.design_jobs}
      label="Design"
      path={paths.design_jobs}
    />
    <LinkChip
      selected={isMarketingRoute(location)}
      label="Marketing"
      path={paths.marketing_jobs}
    />
    <LinkChip
      selected={isManagerRoute(location)}
      label="Manager/Exec"
      path={paths.manager_jobs}
    />
    <LinkChip
      selected={isWritingRoute(location)}
      label="Writing"
      path={paths.writing_jobs}
    />
  </Filter>
);

export default withRouter(AllJobFilters);
