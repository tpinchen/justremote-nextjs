import { Server } from "../../utils/server";
import { BASE_JOBS_API } from "../../constants/paths";
import { paths } from "../../constants/paths";
import {
  postNewJob,
  postNewJobSuccess,
  postNewJobError
} from "../actions/new-job-actions";
import { showLoader, hideLoader } from "../actions/loading-actions";

const sanitizeWebsiteURL = url => {
  let result = url.replace(/(^\w+:|^)\/\//, "");
  return result;
};

const formatJobData = (state, token, provider) => {
  return {
    data: {
      rwa: state.rwa,
      provider: provider,
      stripe_token: token,
      job: {
        title: state.title,
        about_job_company: state.about_job_company,
        about_role: state.about_role,
        remote_type: state.remote_type,
        job_type: state.job_type,
        job_category: state.job_category,
        who_looking_for: state.who_looking_for,
        our_offer: state.our_offer,
        apply: state.apply,
        overlap_required: state.overlap_required,
        overlap_hours: state.overlap_hours,
        specific_country_required: state.specific_country_required,
        country_list: state.specific_country_required ? state.country_list : [],
        technology_list: state.technology_list,
        tool_list: state.tool_list,
        job_city: state.job_city,
        job_county_state_short: state.job_county_state_short,
        job_county_state_long: state.job_county_state_long,
        job_country: state.job_country,
        job_formatted_location: state.job_formatted_location,
        job_latitude: state.job_latitude,
        job_longitude: state.job_longitude,
        remote_days_count: state.remote_days_count
      },
      company: {
        company_name: state.company_name,
        about_company: state.about_company,
        company_website: sanitizeWebsiteURL(state.company_url),
        company_logo: state.company_logo.encoded,
        company_headquarters: state.company_headquarters,
        company_type: state.company_type,
        company_city: state.company_city,
        company_county_state_short: state.company_county_state_short,
        company_county_state_long: state.company_county_state_long,
        company_country: state.company_country,
        company_formatted_location: state.company_formatted_location,
        company_latitude: state.company_latitude,
        company_longitude: state.company_longitude
      },
      contact: {
        contact_first_name: state.contact_first_name,
        contact_surname: state.contact_surname,
        contact_email: state.contact_email
      }
    }
  };
};

export const submitNewJob = (job, push, token, total_price) => dispatch => {
  let formattedJob = formatJobData(job, token, total_price);

  dispatch(postNewJob());
  dispatch(showLoader());

  return Server({
    method: "post",
    url: BASE_JOBS_API,
    data: formattedJob
  })
    .then(function(response) {
      console.log(response);
      dispatch(postNewJobSuccess());
      dispatch(hideLoader());
      push(paths.thank_you);
    })
    .catch(function(error) {
      console.log(error);
      dispatch(postNewJobError(error));
      dispatch(hideLoader());
    });
};
