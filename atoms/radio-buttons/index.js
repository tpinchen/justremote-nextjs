import React, { Component } from "react";
import styled from "styled-components";
import { ErrorMessage } from "../error-message";

const StyledLabel = styled.label`
  margin-bottom: 10px;
  display: inline-flex;
  margin-right: 15px;

  & input {
    margin: 5px;
  }

  &:last-child {
    margin-bottom: 0;
  }
`;

const StyledTitle = styled.span`
  text-transform: capitalize;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: start;
  flex-wrap: wrap;
`;

export class RadioButtons extends Component {
  constructor(props) {
    super(props);

    let defaultValue;

    // FIXME: This needs refactoring so that it's not being changed in one direction and then back.
    if (this.props.default === true) {
      defaultValue = "Yes";
    } else if (this.props.default === false) {
      defaultValue = "No";
    } else {
      defaultValue = this.props.default;
    }

    this.state = {
      checked: defaultValue
    };

    this.handleChange = this.handleChange.bind(this);
  }

  setDataValue(option) {
    if (option === "Yes") return true;
    if (option === "No") return false;
    return option;
  }

  handleChange(event) {
    this.setState(
      {
        checked: event.target.value
      },
      () => {
        this.props.updateState &&
          this.props.updateState(this.setDataValue(this.state.checked));
      }
    );
  }

  render() {
    const { options, name } = this.props;

    return (
      <div>
        <Wrapper>
          {options &&
            options.map((option, i) => {
              return (
                <StyledLabel key={`${name}-${option}-${i}`}>
                  <input
                    type="radio"
                    value={option}
                    checked={this.state.checked === option}
                    onChange={this.handleChange}
                  />
                  <StyledTitle>{option}</StyledTitle>
                </StyledLabel>
              );
            })}
        </Wrapper>
        {this.props.validate ? (
          <ErrorMessage message={this.props.validate.message} />
        ) : null}
      </div>
    );
  }
}
