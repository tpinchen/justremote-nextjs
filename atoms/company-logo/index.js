import React from "react";
import styled from "styled-components";

const CompanyLogoWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 60px;
  min-height: 60px;

  img {
    border-radius: 3px;
  }
`;

export const CompanyLogo = props => {
  return (
    <div>
      {props.src && props.src.length > 0 ? (
        <CompanyLogoWrapper>
          <img src={props.src} alt={props.alt} />
        </CompanyLogoWrapper>
      ) : null}
    </div>
  );
};
