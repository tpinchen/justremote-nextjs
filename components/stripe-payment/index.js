import React, { Component } from "react";
import { StripeProvider, Elements } from "react-stripe-elements";
import { InjectedPaymentForm } from "../injected-payment-form";
import { setStripePublicKey } from "../../constants/integrations";

export class StripePayment extends Component {
  constructor() {
    super();
    this.state = { stripe: null };
  }

  componentDidMount() {
    if (window.Stripe) {
      this.setState({ stripe: window.Stripe(setStripePublicKey()) });
    } else {
      document.querySelector("#stripe-js").addEventListener("load", () => {
        this.setState({ stripe: window.Stripe(setStripePublicKey()) });
      });
    }
  }

  render() {
    const { stripe } = this.state;
    const { formState, pushLocation, setValidation, submitNewJob } = this.props;
    return (
      <StripeProvider stripe={stripe}>
        <Elements>
          <InjectedPaymentForm
            formState={formState}
            submitNewJob={submitNewJob}
            pushLocation={pushLocation}
            setValidation={setValidation}
          />
        </Elements>
      </StripeProvider>
    );
  }
}
