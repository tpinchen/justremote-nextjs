import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getCopywriterJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return (
      title.indexOf("copywriter") >= 0 || title.indexOf("copywriting") >= 0
    );
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.writing.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.writing.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="copywriter"
      canonical={`https://justremote.co${paths.copywriter_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Copywriter Jobs - JustRemote"
      pageDesc="Remote Copywriter Jobs from around the globe. Freelance and full time writing jobs for copywriters, content writers and more."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getCopywriterJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
