import React, { Component } from "react";
import styled from "styled-components";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";
import { ErrorMessage } from "../../atoms/error-message";

const Input = styled.input`
  appearance: none;
  padding: 10px;
  font-size: 16px;
  border: 1px solid ${props => (props.isInvalid ? "red" : "#ddd")};
  width: 100%;
  outline: none;
  border-radius: 3px;
  height: 50px;
`;

const Suggestions = styled.div`
  padding: 10px;
`;

export class PlacesTypeahead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: "",
      formatted_address: ""
    };
  }

  handleChange = address => {
    this.setState({ address }, () => {
      if (this.state.address !== this.state.formatted_address) {
        this.props.validateLocation("");
      } else if (this.state.address.length === 0) {
        this.props.validateLocation("");
      } else {
        this.props.validateLocation(true);
      }
    });
  };

  handleSelect = address => {
    let location = {
      city: "",
      county_state_long: "",
      county_state_short: "",
      country: "",
      formatted_location: "",
      latitude: "",
      longitude: ""
    };

    geocodeByAddress(address)
      .then(results => {
        const addressAttr = results[0].address_components;
        const countyPosition = () => {
          if (addressAttr.length === 5) {
            return 3;
          } else if (addressAttr.length === 1) {
            return 1; // This is for singapore/Hong Kong where there are single results
          } else {
            return 2;
          }
        };

        const countryPosition = () => {
          if (addressAttr[addressAttr.length - 1].types[0] === "country") {
            return 1;
          } else {
            return 2;
          }
        };
        location.formatted_location = results[0].formatted_address;

        this.setState({
          address: location.formatted_location,
          formatted_address: location.formatted_location
        });

        location.city = addressAttr[0].long_name;
        location.county_state_long =
          addressAttr[addressAttr.length - countyPosition()].long_name;
        location.county_state_short =
          addressAttr[addressAttr.length - countyPosition()].short_name;
        location.country =
          addressAttr[addressAttr.length - countryPosition()].long_name;
        return getLatLng(results[0]);
      })
      .then(latLng => {
        location.latitude = latLng.lat.toString();
        location.longitude = latLng.lng.toString();
        this.props.setLocation(location);
        this.props.validateLocation(true);
      })
      .catch(error => console.error("Error", error));
  };

  render() {
    const searchOptions = {
      types: ["(cities)"]
    };

    return (
      <div>
        <PlacesAutocomplete
          value={this.state.address}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
          searchOptions={searchOptions}
        >
          {({ getInputProps, suggestions, getSuggestionItemProps }) => (
            <div>
              <Input
                isInvalid={
                  this.props.validate ? this.props.validate.isInvalid : null
                }
                {...getInputProps({
                  placeholder: "Search Cities ..."
                })}
              />
              <div className="autocomplete-dropdown-container">
                {suggestions.map(suggestion => {
                  const className = suggestion.active
                    ? "suggestion-item--active"
                    : "suggestion-item";
                  const style = suggestion.active
                    ? { backgroundColor: "#fafafa", cursor: "pointer" }
                    : { backgroundColor: "#ffffff", cursor: "pointer" };
                  return (
                    <Suggestions
                      {...getSuggestionItemProps(suggestion, {
                        className,
                        style
                      })}
                    >
                      <span>{suggestion.description}</span>
                    </Suggestions>
                  );
                })}
              </div>
            </div>
          )}
        </PlacesAutocomplete>
        {this.props.validate ? (
          <ErrorMessage message={this.props.validate.message} />
        ) : null}
      </div>
    );
  }
}
