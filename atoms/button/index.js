import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { colors } from "../../constants/theme";

const StyledButton = styled.div`
  text-align: ${(props) => (props.align ? props.align : "left")};

  a {
    display: inline-block;
    background-color: ${colors.primaryRed};
    color: ${colors.white};
    padding: 16px 24px;
    width: ${(props) => (props.fullWidth ? "100%" : "auto")};
    text-align: ${(props) => (props.fullWidth ? "center" : "initial")};
    text-align: ${(props) => (props.align ? props.align : "left")};
    border-radius: 3px;
    text-decoration: none;
    user-select: none;

    &:visited {
      color: ${colors.white};
    }

    &:hover {
      cursor: pointer;
    }
  }
`;

export const Button = (props) => {
  return (
    <StyledButton
      isGreen={props.isGreen}
      fullWidth={props.fullWidth}
      align={props.align}
    >
      <a href={props.path} onClick={props.click}>
        {props.title}
      </a>
    </StyledButton>
  );
};

export const LinkButton = (props) => {
  return (
    <StyledButton
      isGreen={props.isGreen}
      fullWidth={props.fullWidth}
      align={props.align}
      small={props.small}
    >
      <Link href={props.path}>{props.title}</Link>
    </StyledButton>
  );
};

export const ExternalLinkButton = (props) => {
  return (
    <StyledButton
      fullWidth={props.fullWidth}
      align={props.align}
      small={props.small}
      isGreen={props.isGreen}
    >
      <a href={props.path} target="_blank" rel="noopener">
        {props.title}
      </a>
    </StyledButton>
  );
};
