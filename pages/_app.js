import React from "react";
import App from "next/app";
import Head from "next/head";
import withReduxStore from "../lib/with-redux-store";
import { Provider } from "react-redux";
import { GlobalStyle } from "../components/global-style";
import { withRouter } from "next/dist/client/router";
import { Navbar } from "../components/navbar";
import { EmailFormContainer } from "../components/email-form/emailFormContainer";
import { LoaderContainer } from "../components/loader/loaderContainer";
import { Modal } from "../components/modal";
import { Footer } from "../components/footer";

class MyApp extends App {
  render() {
    const BASE_URL =
      process.env.NODE_ENV === "production"
        ? "https://justremote.co"
        : "local.justremote.co:3000";
    const { Component, pageProps, store, router } = this.props;
    return (
      <Provider store={store}>
        <Head>
          <meta property="og:url" content={`${BASE_URL}${router.pathname}`} />
          <meta property="og:type" content="website" />
        </Head>
        <GlobalStyle />
        <Navbar />
        <Component {...pageProps} />
        <Footer />
        <EmailFormContainer />
        <LoaderContainer />
        <Modal />
      </Provider>
    );
  }
}

export default withReduxStore(withRouter(MyApp));
