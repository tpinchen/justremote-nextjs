import React from "react";
import styled from "styled-components";
import { Chip } from "../../atoms/chip";

const StyledChipBlock = styled.div``;
const ChipBlockTitle = styled.div`
  font-size: 16px;
  margin-bottom: 15px;
  font-weight: 600;
`;

export const ChipBlock = props => {
  return (
    <StyledChipBlock style={props.styles}>
      <ChipBlockTitle>{props.title}</ChipBlockTitle>
      {props.chips.map((chip, i) => {
        return <Chip key={i} label={chip.label} />;
      })}
    </StyledChipBlock>
  );
};
