import { ShowJobs } from "../components/show-jobs";
import { useSelector } from "react-redux";
import { paths, BASE_JOBS_API } from "../constants/paths";
import { Server } from "../utils/server";

const getEditingJobs = (jobs) => {
  return jobs.filter((job) => {
    const title = job.title.toLowerCase();
    return title.indexOf("editing") >= 0 || title.indexOf("editor") >= 0;
  });
};

const Index = ({ jobs }) => {
  const filters = useSelector((state) => state.filterState.writing.filters);
  const filtersApplied = useSelector(
    (state) => state.filterState.writing.filtersApplied
  );
  const favourited = useSelector((state) => state.favouriteState.favourites);

  return (
    <ShowJobs
      category="editing"
      canonical={`https://justremote.co${paths.editing_jobs}`}
      jobs={jobs}
      filters={filters}
      filtersApplied={filtersApplied}
      favourited={favourited}
      pageTitle="Remote Editing Jobs - JustRemote"
      pageDesc="Remote Editing Jobs from around the globe. Freelance and full time writing jobs for copywriters, content writers and more."
    />
  );
};

export async function getStaticProps() {
  const response = await Server({
    method: "get",
    url: BASE_JOBS_API,
  });

  const jobs = getEditingJobs(response.data);

  return {
    props: {
      jobs,
    },
  };
}

export default Index;
