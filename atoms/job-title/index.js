import React from "react";
import styled from "styled-components";
import { media } from "../../constants/theme";

const StyledJobTitle = styled.h1`
  font-size: 28px;
  margin-bottom: 5px;
  letter-spacing: -0.03em;
  text-align: center;

  ${media.desktop`
    font-size: 50px;
    letter-spacing: -0.05em;
    text-align:left;
  `}
`;

export const JobTitle = (props) => {
  return <StyledJobTitle>{props.title}</StyledJobTitle>;
};
