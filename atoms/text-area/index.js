import React from "react";
import styled from "styled-components";
import { ErrorMessage } from "../error-message";

const StyledTextArea = styled.textarea`
  padding: 10px;
  font-size: 16px;
  border: 1px solid ${props => (props.isInvalid ? "red" : "#ddd")};
  resize: none;
  box-shadow: none;
  width: 100%;
`;

export const TextArea = props => {
  return (
    <div>
      <StyledTextArea
        isInvalid={props.validate ? props.validate.isInvalid : null}
        value={props.value}
        onChange={props.change}
        rows={props.rows}
      />
      {props.validate ? (
        <ErrorMessage message={props.validate.message} />
      ) : null}
    </div>
  );
};
